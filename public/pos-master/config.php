<?php
/**
 * Created by PhpStorm.
 * User: Mehmet HAKKIOĞLU <mehmethakkioglu@yandex.com>
 * Date: 10.02.2020
 * Time: 22:49
 * Description:
 */
require __DIR__.'/vendor/autoload.php';

$host_url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http') . "://$_SERVER[HTTP_HOST]";
$path = '/pos-test/';
$base_url = $host_url . $path;

$request = \Symfony\Component\HttpFoundation\Request::createFromGlobals();
$ip = $request->getClientIp();

// API kullanıcı bilgileri
$account = [
    'bank'          => 'isbank',
    'model'         => 'regular',
    'client_id'     => '700664955275',
    'username'      => 'aboneadmin',
    'password'      => 'BVBD7417',
    'env'           => 'production', // test veya production. test ise; API Test Url, production ise; API Production URL kullanılır.
];

// API kullanıcı hesabı ile paket bir değişkene aktarılıyor
try {
    $pos = new \Mews\Pos\Pos($account);
} catch (\Mews\Pos\Exceptions\BankNotFoundException $e) {
    var_dump($e->getCode(), $e->getMessage());
    exit();
} catch (\Mews\Pos\Exceptions\BankClassNullException $e) {
    var_dump($e->getCode(), $e->getMessage());
    exit();
}
