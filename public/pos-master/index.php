<?php
/**
 * Created by PhpStorm.
 * User: Mehmet HAKKIOĞLU <mehmethakkioglu@yandex.com>
 * Date: 10.02.2020
 * Time: 22:49
 * Description:
 */
require 'config.php';

// Sipariş bilgileri
$order = [
    'id'            => '444DERGI-'.uniqid(),
    'name'          => 'DENİZ OKUMUŞ', // zorunlu değil
    'email'         => 'mehmet@abonesepeti.com', // zorunlu değil
    //'user_id'       => '12', // zorunlu değil
    'amount'        => (double) 5, // Sipariş tutarı
    'installment'   => '0',
    'currency'      => 'TRY',
    'ip'            => $ip,
    'transaction'   => 'pay', // pay => Auth, pre PreAuth (Direkt satış için pay, ön provizyon için pre)
];

// Kredi kartı bilgieri
$card = [
    'number'        => '4508031367256579', // Kredi kartı numarası
    'month'         => '11', // SKT ay
    'year'          => '24', // SKT yıl, son iki hane
    'cvv'           => '846', // Güvenlik kodu, son üç hane
];

// API kullanıcısı ile oluşturulan $pos değişkenine prepare metoduyla sipariş bilgileri gönderiliyor
$pos->prepare($order);

// Ödeme tamamlanıyor
$payment = $pos->payment($card);

// Ödeme başarılı mı?
$payment->isSuccess();
//veya
$pos->isSuccess();

// Ödeme başarısız mı?
$payment->isError();
//veya
$pos->isError();

// Sonuç çıktısı
print_r($payment->response);
