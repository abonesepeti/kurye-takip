<?php
/**
 * Time: 13:22
 */
namespace Carrier\Repository;

use Carrier\Model\CarrierCore;
use Exception;
use Follow\Model\FollowCore;
use Zend\Db\TableGateway\TableGateway;

class CarrierCoreTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function carrierList()
    {
        $dataList = [];
        $rowset = $this->tableGateway->select('status="Y"');

        foreach ($rowset as $item)
        {
            $dataList[$item->id] = [
                'id' => $item->id,
                'name_surname' => $item->nameSurname,
                'gsm' => $item->gsm,
                'address' => $item->address,
                'area' => $item->area,
                'price' => $item->price,
                'status' => $item->status,
                'ex_date' => $item->exDate,
                'user_id' => $item->userID,
                'dealers' => $item->dealers,
                'tc' => $item->tc,
                'create_date' => $item->createDate,
                'update_date' => $item->updateDate
            ];
        }

        return $dataList;
    }

    public function findByOneCarrier(array $data)
    {
        $dataList = [];
        $rowset = $this->tableGateway->select($data);

        foreach ($rowset as $item)
        {
            $dataList = [
                'id' => $item->id,
                'name_surname' => $item->nameSurname,
                'gsm' => $item->gsm,
                'address' => $item->address,
                'area' => $item->area,
                'price' => $item->price,
                'status' => $item->status,
                'ex_date' => $item->exDate,
                'user_id' => $item->userID,
                'dealers' => $item->dealers,
                'tc' => $item->tc,
                'create_date' => $item->createDate,
                'update_date' => $item->updateDate
            ];
        }

        return $dataList;
    }

    public function saveCarrier(CarrierCore $item)
    {
        $data = [
            'name_surname' => $item->nameSurname,
            'gsm' => $item->gsm,
            'address' => $item->address,
            'area' => $item->area,
            'price' => $item->price,
            'status' => $item->status,
            'ex_date' => $item->exDate,
            'user_id' => $item->userID,
            'dealers' => $item->dealers,
            'tc' => $item->tc,
            'create_date' => $item->createDate,
            'update_date' => $item->updateDate
        ];

        try{
            $result =  $this->tableGateway->insert($data);
            if($result)
            {
                return true;
            }else{
                return false;
            }
        }catch (Exception $e)
        {
            return $e->getMessage();
        }
    }

    public function removeCarrier($id)
    {
        $result = $this->tableGateway->update([
            'status' => 'N'
        ], ['id' => $id]);
        if($result)
        {
            return true;
        }else{
            return false;
        }
    }

    public function updateCarrier(array $data,$id)
    {
        $result = $this->tableGateway->update($data, ['id' => $id]);
        if($result)
        {
            return true;
        }else{
            return false;
        }
    }
}
