<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Carrier;

use Carrier\Model\CarrierCore;
use Carrier\Repository\CarrierCoreTable;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ModuleManager\Feature\ConfigProviderInterface;

class Module implements ConfigProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                CarrierCoreTable::class => function ($container) {
                    $tableGateway = $container->get('Repository\CarrierCoreTableGateway');
                    $table = new CarrierCoreTable($tableGateway);
                    return $table;
                },
                'Repository\CarrierCoreTableGateway' => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new CarrierCore());
                    return new TableGateway('carrier_core', $dbAdapter, null, $resultSetPrototype);
                },
            ],
        ];
    }
}
