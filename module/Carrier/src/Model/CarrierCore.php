<?php
/**
 * Time: 22:49
 * Description:
 */

namespace Carrier\Model;

class CarrierCore
{
    public $id;
    public $nameSurname;
    public $tc;
    public $gsm;
    public $address;
    public $area;
    public $status;
    public $price;
    public $exDate;
    public $userID;
    public $dealers;
    public $createDate;
    public $updateDate;

    public function exchangeArray($data)
    {
        $this->id  = (!empty($data['id'])) ? $data['id'] : null;
        $this->nameSurname = (!empty($data['name_surname'])) ? $data['name_surname'] : null;
        $this->tc = (!empty($data['tc'])) ? $data['tc'] : null;
        $this->gsm = (!empty($data['gsm'])) ? $data['gsm'] : null;
        $this->address = (!empty($data['address'])) ? $data['address'] : null;
        $this->area = (!empty($data['area'])) ? $data['area'] : null;
        $this->price = (!empty($data['price'])) ? $data['price'] : null;
        $this->status = (!empty($data['status'])) ? $data['status'] : null;
        $this->exDate = (!empty($data['ex_date'])) ? $data['ex_date'] : null;
        $this->userID = (!empty($data['user_id'])) ? $data['user_id'] : null;
        $this->dealers = (!empty($data['dealers'])) ? $data['dealers'] : null;
        $this->createDate = (!empty($data['create_date'])) ? $data['create_date'] : null;
        $this->updateDate = (!empty($data['update_date'])) ? $data['update_date'] : null;
    }

    public function getArrayCopy()
    {
        return [
            'id' => $this->id,
            'name_surname' => $this->nameSurname,
            'tc' => $this->tc,
            'gsm' => $this->gsm,
            'address' => $this->address,
            'area' => $this->area,
            'price' => $this->price,
            'status' => $this->status,
            'ex_date' => $this->exDate,
            'user_id' => $this->userID,
            'dealers' => $this->dealers,
            'create_date' => $this->createDate,
            'update_date' => $this->updateDate
        ];
    }
}
