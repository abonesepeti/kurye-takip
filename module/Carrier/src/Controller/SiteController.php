<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Carrier\Controller;

use Carrier\Repository\CarrierCoreTable;
use Core\Repository\CityTable;
use Core\Repository\CountyTable;
use User\Repository\UserCoreTable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class SiteController extends AbstractActionController
{
    /** @var $userCoreTable UserCoreTable */
    private $userCoreTable;

    /** @var $cityTable CityTable */
    private $cityTable;

    /** @var $carrierCoreTable CarrierCoreTable */
    private $carrierCoreTable;

    /** @var $countyTable CountyTable */
    private $countyTable;

    public function __construct()
    {
        $args = func_get_args();
        $this->userCoreTable = $args[0];
        $this->cityTable = $args[1];
        $this->carrierCoreTable = $args[2];
        $this->countyTable = $args[3];
    }

    public function carrierAction()
    {
        $title = 'Kurye Yönetimi';
        $options = [
            'title' => $title,
            'subtitle' => '',
            'breadcrumb' => [
                [
                    'title' => 'Ana Sayfa',
                    'route' => 'site'
                ],
                [
                    'title' => 'Kurye Yönetimi',
                    'route' => 'site/carrier'
                ]
            ]
        ];

        $dataList = [];
        $userList = [];

        foreach ($this->userCoreTable->myUsers([
            'parent_id' => $_SESSION['userInfo']['id']
        ]) as $item)
        {
            $userList[$item['id']] = $item['id'];
        }

        foreach ($this->carrierCoreTable->carrierList() as $item)
        {
            if($_SESSION['userInfo']['type'] == 'admin')
            {
                $dataList[] = $item;
            }elseif($_SESSION['userInfo']['type'] == 'user')
            {
                if($userList[$item['user_id']])
                {
                    $dataList[] = $item;
                }
            }
        }

        $view = new ViewModel([
            'options' => $options,
            'dataList' => $dataList
        ]);
        $view->setTemplate('page/site/carrier-index');
        $this->layout()->setTemplate('layout/layout')->setVariables([
            'title' => $title,
            'navbar' => 'carrier'
        ]);

        $view->setVariables([
            'userInfo' => $this->userCoreTable->findByOneUser([
                'id' => $_SESSION['userInfo']['id']
            ])
        ]);

        if($_SESSION['loginControl'] == true)
        {
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user/login');
        }
    }

    public function createAction()
    {
        $title = 'Kurye Oluştur';
        $options = [
            'title' => $title,
            'subtitle' => 'Kuryenizi bu sayfadan oluşturabilirsiniz.',
            'breadcrumb' => [
                [
                    'title' => 'Ana Sayfa',
                    'route' => 'site'
                ],
                [
                    'title' => 'Kurye Yönetimi',
                    'route' => 'site/carrier'
                ],
                [
                    'title' => 'Kurye Oluştur',
                    'route' => 'site/carrier/create'
                ]
            ],
            'button' => 'Kaydet',
            'color' => 'primary',
            'icon' => 'fa fa-save',
            'saveURL' => 'api/carrier/create',
            'cityList' => $this->cityTable->cityList()
        ];

        $dealerList = [];
        foreach ($this->userCoreTable->userList() as $item)
        {
            if($item['type'] != 'carrier')
            {
                $dealerList[] = $item;
            }
        }

        $view = new ViewModel([
            'options' => $options,
            'dealers' => $dealerList
        ]);
        $view->setTemplate('page/site/carrier-create');
        $this->layout()->setTemplate('layout/layout')->setVariables([
            'title' => $title,
            'navbar' => 'carrier'
        ]);

        $view->setVariables([
            'userInfo' => $this->userCoreTable->findByOneUser([
                'id' => $_SESSION['userInfo']['id']
            ])
        ]);

        if($_SESSION['loginControl'] == true)
        {
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user/login');
        }
    }

    public function editAction()
    {
        $id = $this->params()->fromRoute('id');

        $title = 'Kurye Düzenle';
        $options = [
            'title' => $title,
            'subtitle' => 'Kuryenizi bu sayfadan düzenleyebilirsiniz.',
            'breadcrumb' => [
                [
                    'title' => 'Ana Sayfa',
                    'route' => 'site'
                ],
                [
                    'title' => 'Kurye Yönetimi',
                    'route' => 'site/carrier'
                ],
                [
                    'title' => 'Kurye Düzenle',
                    'route' => 'site/carrier'
                ]
            ],
            'button' => 'Düzenle',
            'color' => 'primary',
            'icon' => 'fa fa-edit',
            'saveURL' => 'api/carrier/edit',
            'cityList' => $this->cityTable->cityList()
        ];

        $data = $this->carrierCoreTable->findByOneCarrier([
            'id' => $id
        ]);

        $userDetail = $this->userCoreTable->findByOneUser([
            'id' => $data['user_id']
        ]);

        $data = array_merge($data,[
            'username' => $userDetail['username'],
            'user_id' => $userDetail['id']
        ]);

        $area = json_decode($data['area'],true);

        $countyList = [
            'countyOne' => $this->countyTable->countyToCity($area[0][0]),
            'countyTwo' => $this->countyTable->countyToCity($area[1][0]),
            'countyThree' => $this->countyTable->countyToCity($area[2][0]),
            'countyFour' => $this->countyTable->countyToCity($area[3][0])
        ];

        $dealerList = [];
        foreach ($this->userCoreTable->userList() as $item)
        {
            if($item['type'] != 'carrier')
            {
                $dealerList[] = $item;
            }
        }

        $view = new ViewModel([
            'options' => $options,
            'data' => $data,
            'countyList' => $countyList,
            'dealers' => $dealerList,
            'countyToCityList' => $this->countyTable->countyToCityList(),
            'id' => $id,
            'user_id' => $data['user_id']
        ]);
        $view->setTemplate('page/site/carrier-edit');
        $this->layout()->setTemplate('layout/layout')->setVariables([
            'title' => $title,
            'navbar' => 'carrier'
        ]);

        $view->setVariables([
            'userInfo' => $this->userCoreTable->findByOneUser([
                'id' => $_SESSION['userInfo']['id']
            ])
        ]);

        if($_SESSION['loginControl'] == true)
        {
            if($_SESSION['userInfo']['type'] == 'admin')
            {
                return $view;
            }elseif($_SESSION['userInfo']['type'] == 'user'){
                if($this->userCoreTable->findByOneUser([
                    'id' => $data['user_id']
                ])['parent_id'] == $_SESSION['userInfo']['id'])
                {
                    return $view;
                }else{
                    return $this->redirect()->toRoute('site/carrier');
                }
            }else{
                return $this->redirect()->toRoute('site/carrier');
            }
        }else{
            return $this->redirect()->toRoute('site/user/login');
        }
    }
}
