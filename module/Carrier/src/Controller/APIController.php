<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Carrier\Controller;

use Carrier\Model\CarrierCore;
use Carrier\Repository\CarrierCoreTable;
use Core\Classes\MethaClasses;
use User\Model\UserCore;
use User\Repository\UserCoreTable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class APIController extends AbstractActionController
{
    /** @var $userCoreTable UserCoreTable */
    private $userCoreTable;

    /** @var $carrirerCoreTable CarrierCoreTable */
    private $carrirerCoreTable;

    /** @var $methaClasses MethaClasses */
    private $methaClasses;

    public function __construct()
    {
        $args = func_get_args();
        $this->userCoreTable = $args[0];
        $this->carrirerCoreTable = $args[1];

        $this->methaClasses = new MethaClasses();
    }

    public function carrierAction()
    {
        $view = new ViewModel();

        if ($this->getRequest()->isPost())
        {
            $formData = $this->params()->fromPost();
            if($formData['type'] == 'single')
            {
                $view->setVariables([
                    'content' => [
                        'code' => 200,
                        'message' => 'Başarılı',
                        'result' => $this->carrirerCoreTable->findByOneCarrier([
                            'id' => $formData['id']
                        ])
                    ]
                ]);
            }elseif($formData['type'] == 'all')
            {
                $view->setVariables([
                    'content' => [
                        'code' => 200,
                        'message' => 'Başarılı',
                        'result' => $this->carrirerCoreTable->carrierList()
                    ]
                ]);
            }else{
                $view->setVariables([
                    'content' => [
                        'code' => 201,
                        'message' => 'Geçersiz parametre.'
                    ]
                ]);
            }
        }else{
            $view->setVariables([
                'content' => [
                    'code' => 403,
                    'message' => 'Yetkisiz erişim.'
                ]
            ]);
        }

        $view->setTemplate('page/site/json');
        $this->layout()->setTemplate('layout/json_layout');

        return $view;
    }

    public function createAction()
    {
        $view = new ViewModel();

        if ($this->getRequest()->isPost())
        {
            $formData = $this->params()->fromPost();

            $areaList = [
                $formData['area_one'],
                $formData['area_two'],
                $formData['area_three'],
                $formData['area_four']
            ];

            $priceList = [];
            foreach ($this->methaClasses->priceTypes() as $item)
            {
                $priceList[$item['name']] = $formData[$item['name']];
            }

            $formData = array_merge($formData,[
                'area' => json_encode($areaList),
                'status' => 'Y',
                'create_date' => date('Y-m-d H:i:s'),
                'update_date' => date('Y-m-d H:i:s'),
                'password' => hash('sha256',md5($formData['password'])),
                'type' => 'carrier',
                'parent_id' => isset($_SESSION['userInfo']['id']) ? $_SESSION['userInfo']['id'] : $formData['parent_id'],
                'price' => json_encode($priceList),
                'dealers' => json_encode($formData['dealers']),
                'tc' => $formData['tc']
            ]);

            unset($formData['area_one']);
            unset($formData['area_two']);
            unset($formData['area_three']);
            unset($formData['area_four']);

            $userCore = new UserCore();
            $userCore->exchangeArray($formData);

            $userControl = $this->userCoreTable->findByOneUser([
                'username' => $formData['username']
            ]);

            if(count($userControl) > 0)
            {
                $view->setVariables([
                    'content' => [
                        'code' => 201,
                        'message' => $formData['username'].' isimli kullanıcı adı sistemde kayıtlıdır.'
                    ]
                ]);
            }else{
                $userResult = $this->userCoreTable->saveUser($userCore);
                if($userResult)
                {
                    $formData = array_merge($formData,[
                        'user_id' => $userResult
                    ]);

                    $carrierCore = new CarrierCore();
                    $carrierCore->exchangeArray($formData);

                    $result = $this->carrirerCoreTable->saveCarrier($carrierCore);

                    if($result)
                    {
                        $view->setVariables([
                            'content' => [
                                'code' => 200,
                                'message' => 'Kurye ekleme işleminiz başarılı bir şekilde gerçekleştirilmiştir.'
                            ]
                        ]);
                    }else{
                        $view->setVariables([
                            'content' => [
                                'code' => 201,
                                'message' => 'Kurye ekleme işleminiz sırasında bir hata ile karşışaltık.Lütfen daha sonra tekrar deneyiniz.'
                            ]
                        ]);
                    }
                }else{
                    $view->setVariables([
                        'content' => [
                            'code' => 202,
                            'message' => 'Kurye ekleme işleminiz sırasında bir hata ile karşışaltık.Lütfen daha sonra tekrar deneyiniz.'
                        ]
                    ]);
                }
            }
        }else{
            $view->setVariables([
                'content' => [
                    'code' => 403,
                    'message' => 'Yetkisiz erişim.'
                ]
            ]);
        }

        $view->setTemplate('page/site/json');
        $this->layout()->setTemplate('layout/json_layout');

        if($_SESSION['loginControl'] == true)
        {
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user/login');
        }
    }

    public function editAction()
    {
        $view = new ViewModel();

        if ($this->getRequest()->isPost())
        {
            $formData = $this->params()->fromPost();

            $areaList = [
                $formData['area_one'],
                $formData['area_two'],
                $formData['area_three'],
                $formData['area_four']
            ];

            $password = null;

            if(strlen($formData['password']) > 0)
            {
                $password = hash('sha256',md5($formData['password']));
            }else{
                $password = $this->userCoreTable->findByOneUser([
                    'id' => $formData['user_id']
                ])['password'];
            }

            $formData = array_merge($formData,[
                'dealers' => json_encode($formData['dealers']),
                'area' => json_encode($areaList),
                'update_date' => date('Y-m-d H:i:s'),
                'password' => $password,
                'price' => json_encode([
                    'aktif' => $formData['aktif'],
                    'gonder' => $formData['gonderi']
                ]),
                'tc' => $formData['tc'],
            ]);

            unset($formData['area_one']);
            unset($formData['area_two']);
            unset($formData['area_three']);
            unset($formData['area_four']);

            $result = $this->carrirerCoreTable->updateCarrier([
                'name_surname' => $formData['name_surname'],
                'dealers' => $formData['dealers'],
                'gsm' => $formData['gsm'],
                'address' => $formData['address'],
                'price' => $formData['price'],
                'update_date' => date('Y-m-d H:i:s'),
                'area' => $formData['area'],
                'tc' => $formData['tc'],
            ],[
                'id' => $formData['id']
            ]);

            if($result)
            {
                $this->userCoreTable->updateUser([
                    'username' => $formData['username'],
                    'password' => $password,
                    'update_date' => date('Y-m-d H:i:s')
                ],$formData['user_id']);

                $view->setVariables([
                    'content' => [
                        'code' => 200,
                        'message' => 'Kurye düzenleme işleminiz başarılı bir şekilde gerçekleştirilmiştir.'
                    ]
                ]);
            }else{
                $view->setVariables([
                    'content' => [
                        'code' => 201,
                        'message' => 'Kurye düzenleme işleminiz sırasında bir hata ile karşışaltık.Lütfen daha sonra tekrar deneyiniz.'
                    ]
                ]);
            }
        }else{
            $view->setVariables([
                'content' => [
                    'code' => 403,
                    'message' => 'Yetkisiz erişim.'
                ]
            ]);
        }

        $view->setTemplate('page/site/json');
        $this->layout()->setTemplate('layout/json_layout');

        if($_SESSION['loginControl'] == true)
        {
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user/login');
        }
    }

    public function removeAction()
    {
        $view = new ViewModel();

        if ($this->getRequest()->isPost())
        {
            $formData = $this->params()->fromPost();

            $result = $this->carrirerCoreTable->updateCarrier([
                'status' => 'N',
                'update_date' => date('Y-m-d H:i:s')
            ],$formData['id']);

            if($result)
            {
                $this->userCoreTable->updateUser([
                    'status' => 'N',
                    'update_date' => date('Y-m-d H:i:s')
                ],$this->carrirerCoreTable->findByOneCarrier([
                    'id' => $formData['id']
                ])['user_id']);
                $view->setVariables([
                    'content' => [
                        'code' => 200,
                        'message' => 'Kurye silme işleminiz başarılı bir şekilde gerçekleştirilmiştir.'
                    ]
                ]);
            }else{
                $view->setVariables([
                    'content' => [
                        'code' => 201,
                        'message' => 'Kurye silme işleminiz sırasında bir hata ile karşışaltık.Lütfen daha sonra tekrar deneyiniz.'
                    ]
                ]);
            }
        }else{
            $view->setVariables([
                'content' => [
                    'code' => 403,
                    'message' => 'Yetkisiz erişim.'
                ]
            ]);
        }

        $view->setTemplate('page/site/json');
        $this->layout()->setTemplate('layout/json_layout');

        if($_SESSION['loginControl'] == true)
        {
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user/login');
        }
    }
}
