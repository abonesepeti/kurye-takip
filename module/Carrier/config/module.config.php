<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Carrier;

use Carrier\Repository\CarrierCoreTable;
use Core\Repository\CityTable;
use Core\Repository\CountyTable;
use User\Repository\UserCoreTable;
use Zend\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'site' => [
                'child_routes' => [
                    'carrier' => [
                        'type' => Segment::class,
                        'may_terminate' => true,
                        'options' => [
                            'route'    => 'carrier[/]',
                            'defaults' => [
                                'controller' => Controller\SiteController::class,
                                'action'     => 'carrier',
                            ],
                        ],
                        'child_routes' => [
                            'create' => [
                                'type' => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'create[/]',
                                    'defaults' => [
                                        'controller' => Controller\SiteController::class,
                                        'action'     => 'create',
                                    ],
                                ],
                            ],
                            'edit' => [
                                'type' => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'edit/:id[/]',
                                    'defaults' => [
                                        'controller' => Controller\SiteController::class,
                                        'action'     => 'edit',
                                    ],
                                ],
                            ],
                        ]
                    ],
                ]
            ],
            'panel' => [
                'child_routes' => [
                    'carrier' => [
                        'type'    => Segment::class,
                        'may_terminate' => true,
                        'options' => [
                            'route'    => 'carrier[/]',
                            'defaults' => [
                                'controller' => Controller\PanelController::class,
                                'action'     => 'carrier',
                            ],
                        ],
                    ],
                ]
            ],
            'api' => [
                'child_routes' => [
                    'carrier' => [
                        'type'    => Segment::class,
                        'may_terminate' => true,
                        'options' => [
                            'route'    => 'carrier[/]',
                            'defaults' => [
                                'controller' => Controller\APIController::class,
                                'action'     => 'carrier',
                            ],
                        ],
                        'child_routes' => [
                            'create' => [
                                'type'    => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'create[/]',
                                    'defaults' => [
                                        'controller' => Controller\APIController::class,
                                        'action'     => 'create',
                                    ],
                                ],
                            ],
                            'edit' => [
                                'type'    => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'edit[/]',
                                    'defaults' => [
                                        'controller' => Controller\APIController::class,
                                        'action'     => 'edit',
                                    ],
                                ],
                            ],
                            'remove' => [
                                'type'    => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'remove[/]',
                                    'defaults' => [
                                        'controller' => Controller\APIController::class,
                                        'action'     => 'remove',
                                    ],
                                ],
                            ],
                        ]
                    ],
                ]
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\SiteController::class => function($container) {
                return new Controller\SiteController(
                    $container->get(UserCoreTable::class),
                    $container->get(CityTable::class),
                    $container->get(CarrierCoreTable::class),
                    $container->get(CountyTable::class)
                );
            },
            Controller\PanelController::class => function($container) {
                return new Controller\PanelController();
            },
            Controller\APIController::class => function($container) {
                return new Controller\APIController(
                    $container->get(UserCoreTable::class),
                    $container->get(CarrierCoreTable::class)
                );
            },
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ]
];
