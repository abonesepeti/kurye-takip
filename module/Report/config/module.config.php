<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Report;

use Carrier\Repository\CarrierCoreTable;
use Core\Repository\CityTable;
use Core\Repository\CountyTable;
use Core\Repository\StatusTable;
use Follow\Repository\FollowCoreTable;
use User\Repository\UserCoreTable;
use Zend\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'site'        => [
                'child_routes' => [
                    'report'        => [
                        'type'    => Segment::class,
                        'may_terminate' => true,
                        'options' => [
                            'route'    => 'report[/]',
                            'defaults' => [
                                'controller' => Controller\SiteController::class,
                                'action'     => 'report',
                            ],
                        ],
                        'child_routes' => [
                            'filter'        => [
                                'type'    => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => ':filter[/]',
                                    'defaults' => [
                                        'controller' => Controller\SiteController::class,
                                        'action'     => 'filter',
                                    ],
                                ],
                            ],
                            'progress'        => [
                                'type'    => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'progress[/]',
                                    'defaults' => [
                                        'controller' => Controller\SiteController::class,
                                        'action'     => 'progress',
                                    ],
                                ],
                                'child_routes' => [
                                    'progressFilter'        => [
                                        'type'    => Segment::class,
                                        'may_terminate' => true,
                                        'options' => [
                                            'route'    => ':filter[/]',
                                            'defaults' => [
                                                'controller' => Controller\SiteController::class,
                                                'action'     => 'progressFilter',
                                            ],
                                        ],
                                    ],
                                ]
                            ],
                            'export'        => [
                                'type'    => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'export/:code[/]',
                                    'defaults' => [
                                        'controller' => Controller\SiteController::class,
                                        'action'     => 'export',
                                    ],
                                ],
                            ],
                            'exportDetail'        => [
                                'type'    => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'export-detail/:code[/]',
                                    'defaults' => [
                                        'controller' => Controller\SiteController::class,
                                        'action'     => 'exportDetail',
                                    ],
                                ],
                            ],
                        ]
                    ],
                ]
            ],
            'panel' => [
                'child_routes' => [
                    'report' => [
                        'type'    => Segment::class,
                        'may_terminate' => true,
                        'options' => [
                            'route'    => 'report[/]',
                            'defaults' => [
                                'controller' => Controller\PanelController::class,
                                'action'     => 'report',
                            ],
                        ]
                    ],
                ]
            ],
            'api' => [
                'child_routes' => [
                    'report' => [
                        'type'    => Segment::class,
                        'may_terminate' => true,
                        'options' => [
                            'route'    => 'report[/]',
                            'defaults' => [
                                'controller' => Controller\APIController::class,
                                'action'     => 'report',
                            ],
                        ],
                    ],
                ]
            ],
        ],
    ],

    'controllers' => [
        'factories' => [
            Controller\SiteController::class => function($container) {
                return new Controller\SiteController(
                    $container->get(FollowCoreTable::class),
                    $container->get(CarrierCoreTable::class),
                    $container->get(CityTable::class),
                    $container->get(CountyTable::class),
                    $container->get(StatusTable::class),
                    $container->get(UserCoreTable::class)
                );
            },
            Controller\PanelController::class => function($container) {
                return new Controller\PanelController(
                    $container->get(UserCoreTable::class)
                );
            },
            Controller\APIController::class => function($container) {
                return new Controller\APIController(
                    $container->get(CountyTable::class),
                    $container->get(StatusTable::class),
                    $container->get(FollowCoreTable::class)
                );
            },
        ],
    ],

    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map'             => [
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack'      => [
            __DIR__ . '/../view',
        ],
    ],
];
