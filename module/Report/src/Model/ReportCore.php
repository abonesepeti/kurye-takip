<?php
/**
 * Time: 22:49
 * Description:
 */

namespace Report\Model;

class ReportCore
{
    public $id;
    public $title;

    public function exchangeArray($data)
    {
        $this->id  = (!empty($data['id'])) ? $data['id'] : null;
        $this->title = (!empty($data['title'])) ? $data['title'] : null;
    }

    public function getArrayCopy()
    {
        return [
            'id' => $this->id,
            'title' => $this->title
        ];
    }
}
