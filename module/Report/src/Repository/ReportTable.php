<?php
/**
 * Time: 13:22
 */
namespace Report\Repository;

use DateInterval;
use DatePeriod;
use DateTime;
use Zend\Db\TableGateway\TableGateway;

class ReportTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function reportList()
    {
        $dataList = [];
        $rowset = $this->tableGateway->select();

        foreach ($rowset as $item)
        {
            $dataList[$item->id] = $item->title;
        }

        return $dataList;
    }

    public function reportBetweenList(array $dates,array $filter)
    {
        $period = new DatePeriod(
            new DateTime($dates['start']),
            new DateInterval('P1D'),
            new DateTime($dates['end'])
        );

        $dateList = [];
        foreach ($period as $key => $value)
        {
            $dateList[$value->format('Y-m-d')] = $value->format('Y-m-d');
        }

        $dataList = [];
        $rowset = $this->tableGateway->select($filter);

        foreach ($rowset as $item)
        {
            $date = new \DateTime($item['create_date']);
            if(isset($dateList[$date->format('Y-m-d')]))
            {
                $dataList[$item->id] = $item->title;
            }
        }

        return $dataList;
    }
}
