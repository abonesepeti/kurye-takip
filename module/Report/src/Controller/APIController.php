<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Report\Controller;

use Core\Repository\CountyTable;
use Core\Repository\StatusTable;
use Follow\Repository\FollowCoreTable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class APIController extends AbstractActionController
{
    /** @var $countyTable CountyTable */
    private $countyTable;

    /** @var $status StatusTable */
    private $status;

    /** @var $followCoreTable FollowCoreTable */
    private $followCoreTable;

    public function __construct()
    {
        $args = func_get_args();
        $this->countyTable = $args[0];
        $this->status = $args[1];
        $this->followCoreTable = $args[2];
    }

    public function reportAction()
    {
        $dataList = [];

        foreach ($this->followCoreTable->followList() as $item)
        {
            print_r($item);
        }

        die;
        $view = new ViewModel([
            'content' => $dataList
        ]);
        $view->setTemplate('page/api/json');
        $this->layout()->setTemplate('layout/json_layout');
        return $view;
    }
}
