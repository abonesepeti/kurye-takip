<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Report\Controller;

use Carrier\Repository\CarrierCoreTable;
use Core\Repository\CityTable;
use Core\Repository\CountyTable;
use Core\Repository\StatusTable;
use DateTime;
use Ellumilel\ExcelWriter;
use Follow\Repository\FollowCoreTable;
use User\Repository\UserCoreTable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class SiteController extends AbstractActionController
{
    /** @var $followCoreTable FollowCoreTable */
    private $followCoreTable;

    /** @var $carrierCoreTable CarrierCoreTable */
    private $carrierCoreTable;

    /** @var $city CityTable */
    private $city;

    /** @var $county CountyTable */
    private $county;

    /** @var $status StatusTable */
    private $status;

    /** @var $userCoreTable UserCoreTable */
    private $userCoreTable;

    public function __construct()
    {
        $args = func_get_args();
        $this->followCoreTable = $args[0];
        $this->carrierCoreTable = $args[1];
        $this->city = $args[2];
        $this->county = $args[3];
        $this->status = $args[4];
        $this->userCoreTable = $args[5];
    }

    public function reportAction()
    {
        $title = 'Raporlar';
        $options = [
            'title' => $title,
            'subtitle' => '',
            'breadcrumb' => [
                [
                    'title' => 'Ana Sayfa',
                    'route' => 'site'
                ],
                [
                    'title' => 'Raporlar',
                    'route' => 'site/report'
                ]
            ]
        ];

        $carrierList = $this->carrierCoreTable->carrierList();
        $userList = [];

        foreach ($this->userCoreTable->userList() as $item)
        {
            if($_SESSION['userInfo']['type'] == 'admin')
            {
                if($item['parent_id'] == $_SESSION['userInfo']['id'])
                {
                    $userList[$item['id']] = $item['id'];
                }
            }elseif($_SESSION['userInfo']['type'] == 'user')
            {
                if($item['parent_id'] == $_SESSION['userInfo']['id'])
                {
                    $userList[$item['id']] = $item['id'];
                }
            }
        }

        $carrierListArray = [];

        foreach ($carrierList as $item)
        {
            if($_SESSION['userInfo']['type'] == 'admin')
            {
                $carrierListArray[$item['id']] = $item;
            }elseif($_SESSION['userInfo']['type'] == 'user')
            {
                if(isset($userList[$item['user_id']]))
                {
                    $carrierListArray[$item['id']] = $item;
                }
            }
        }

        $reportList = [];
        $detailList = [];
        foreach ($this->followCoreTable->followList() as $item)
        {
            if($_SESSION['userInfo']['type'] == 'admin')
            {
                $detailList[] = $item;
                $reportList[$carrierListArray[$item['carrier_id']]['name_surname']][$item['status']][$item['id']] = $item['id'];
            }elseif($_SESSION['userInfo']['type'] == 'user')
            {
                if($carrierListArray[$item['carrier_id']])
                {
                    $detailList[] = $item;
                    $reportList[$carrierListArray[$item['carrier_id']]['name_surname']][$item['status']][$item['id']] = $item['id'];
                }
            }
        }

        $view = new ViewModel([
            'options' => $options,
            'reportList' => $reportList,
            'detailList' => $detailList,
            'carrierList' => $this->carrierCoreTable->carrierList(),
            'cityList' => $this->city->cityList(),
            'countyList' => $this->county->cityList(),
            'status' => $this->status->statusDetailList(),
        ]);
        $view->setTemplate('page/site/report-index');
        $this->layout()->setTemplate('layout/layout')->setVariables([
            'title' => $title,
            'navbar' => 'report'
        ]);

        if($_SESSION['loginControl'] == true)
        {
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user/login');
        }
    }

    public function filterAction()
    {
        $filter = $this->params()->fromRoute('filter');

        $title = 'Raporlar';
        $options = [
            'title' => $title,
            'subtitle' => '',
            'breadcrumb' => [
                [
                    'title' => 'Ana Sayfa',
                    'route' => 'site'
                ],
                [
                    'title' => 'Raporlar',
                    'route' => 'site/report'
                ]
            ]
        ];

        $carrierList = $this->carrierCoreTable->carrierList();
        $userList = [];

        foreach ($this->userCoreTable->userList() as $item)
        {
            if($_SESSION['userInfo']['type'] == 'admin')
            {
                if($item['parent_id'] == $_SESSION['userInfo']['id'])
                {
                    $userList[$item['id']] = $item['id'];
                }
            }elseif($_SESSION['userInfo']['type'] == 'user')
            {
                if($item['parent_id'] == $_SESSION['userInfo']['id'])
                {
                    $userList[$item['id']] = $item['id'];
                }
            }
        }

        $carrierListArray = [];

        foreach ($carrierList as $item)
        {
            if($_SESSION['userInfo']['type'] == 'admin')
            {
                $carrierListArray[$item['id']] = $item;
            }elseif($_SESSION['userInfo']['type'] == 'user')
            {
                if(isset($userList[$item['user_id']]))
                {
                    $carrierListArray[$item['id']] = $item;
                }
            }
        }

        $reportList = [];
        foreach ($this->followCoreTable->followBetweebList($filter) as $item)
        {
            if($_SESSION['userInfo']['type'] == 'admin')
            {
                $reportList[$carrierListArray[$item['carrier_id']]['name_surname']][$item['status']][$item['id']] = $item['id'];
                // $reportList[] = $item['id'];
            }elseif($_SESSION['userInfo']['type'] == 'user')
            {
                if($carrierListArray[$item['carrier_id']])
                {
                    $reportList[$carrierListArray[$item['carrier_id']]['name_surname']][$item['status']][$item['id']] = $item['id'];
                }
            }
        }

        $datas = [];

        $dates = explode('&',$filter);

        foreach ($dates as $item)
        {
            $exp = explode('=',$item);

            $datas[$exp[0]] = $exp[1];
        }


        $view = new ViewModel([
            'options' => $options,
            'reportList' => $reportList,
            'status' => $this->status->statusDetailList(),
            'type' => 'filter',
            'datas' => $datas,
            'carrierList' => $carrierList
        ]);
        $view->setTemplate('page/site/report-index');
        $this->layout()->setTemplate('layout/layout')->setVariables([
            'title' => $title,
            'navbar' => 'report'
        ]);

        if($_SESSION['loginControl'] == true)
        {
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user/login');
        }
    }

    public function progressAction()
    {
        $title = 'Hakediş';
        $options = [
            'title' => $title,
            'subtitle' => '',
            'breadcrumb' => [
                [
                    'title' => 'Ana Sayfa',
                    'route' => 'site'
                ],
                [
                    'title' => 'Raporlar',
                    'route' => 'site/report'
                ],
                [
                    'title' => 'Hakediş',
                    'route' => 'site/report/progress'
                ]
            ]
        ];

        $carrierList = $this->carrierCoreTable->carrierList();
        $userList = [];

        foreach ($this->userCoreTable->userList() as $item)
        {
            if($_SESSION['userInfo']['type'] == 'admin')
            {
                if($item['parent_id'] == $_SESSION['userInfo']['id'])
                {
                    $userList[$item['id']] = $item['id'];
                }
            }elseif($_SESSION['userInfo']['type'] == 'user')
            {
                if($item['parent_id'] == $_SESSION['userInfo']['id'])
                {
                    $userList[$item['id']] = $item['id'];
                }
            }
        }

        $carrierListArray = [];

        foreach ($carrierList as $item)
        {
            if($_SESSION['userInfo']['type'] == 'admin')
            {
                $carrierListArray[$item['id']] = $item;
            }elseif($_SESSION['userInfo']['type'] == 'user')
            {
                if(isset($userList[$item['user_id']]))
                {
                    $carrierListArray[$item['id']] = $item;
                }
            }
        }

        $reportList = [];
        foreach ($this->followCoreTable->followList() as $item)
        {
            if($_SESSION['userInfo']['type'] == 'admin' && $item['status'] == 9)
            {
                $reportList[$carrierListArray[$item['carrier_id']]['name_surname']][$item['id']] = $item['price'];
            }elseif($_SESSION['userInfo']['type'] == 'user'  && $item['status'] == 9)
            {
                if($carrierListArray[$item['carrier_id']])
                {
                    $reportList[$carrierListArray[$item['carrier_id']]['name_surname']][$item['id']] = $item['price'];
                }
            }
        }

        $view = new ViewModel([
            'options' => $options,
            'reportList' => $reportList,
            'carrierList' => $this->carrierCoreTable->carrierList(),
            'status' => $this->status->statusDetailList(),
        ]);
        $view->setTemplate('page/site/report-progress');
        $this->layout()->setTemplate('layout/layout')->setVariables([
            'title' => $title,
            'navbar' => 'progress'
        ]);

        if($_SESSION['loginControl'] == true)
        {
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user/login');
        }
    }

    public function exportAction()
    {
        $code = $this->params()->fromRoute('code');

        if($_SESSION['loginControl'] == true)
        {
            $header = [
                'Kurye' => 'string',
                'Adet' => 'string',
                'Hakediş' => 'string'
            ];

            $wExcel = new ExcelWriter();
            $wExcel->writeSheetHeader('Sheet1', $header);
            $wExcel->setAuthor('Mehmet HAKKIOĞLU');
            foreach (json_decode(base64_decode($code),true) as $item)
            {
                $wExcel->writeSheetRow('Sheet1', [
                    $item['name_surname'],
                    $item['quantity'],
                    $item['price'],
                ]);
            }
            $name = date('YmdHis').'.xlsx';
            $file = __DIR__.'/../../../../public/export/'.$name;
            $wExcel->writeToFile($file);
            chmod($file,0777);

            return $this->redirect()->toUrl('http://abonesepeti.org/public/export/'.$name);

            $view = new ViewModel([
                'content' => base64_decode($code)
            ]);
            $view->setTemplate('page/api/csv');
            $this->layout()->setTemplate('layout/csv_layout')->setVariables([
                'title' => date('YmdHis')
            ]);

            return $view;
        }else{
            return $this->redirect()->toRoute('site/user/login');
        }
    }

    public function exportDetailAction()
    {
        $code = $this->params()->fromRoute('code');

        if($_SESSION['loginControl'] == true)
        {
            $header = [
                'Adı Soyadı' => 'string',
                'İl' => 'string',
                'İlçe' => 'string',
                'Adres' => 'string',
                'Paket' => 'string',
                'Taşınacak Numara' => 'string',
                'Kurye' => 'string',
                'Ödeme Durumu' => 'string',
                'İşlem Durumu' => 'string',
            ];

            $wExcel = new ExcelWriter();
            $wExcel->writeSheetHeader('Sheet1', $header);
            $wExcel->setAuthor('Mehmet HAKKIOĞLU');
            foreach (json_decode(base64_decode($code),true) as $item)
            {
                $wExcel->writeSheetRow('Sheet1', [
                    $item['name_surname'],
                    $item['city'],
                    $item['county'],
                    $item['address'],
                    $item['package'],
                    $item['number'],
                    $item['carrier'],
                    $item['pay'],
                    $item['status'],
                ]);
            }
            $name = date('YmdHis').'.xlsx';
            $file = __DIR__.'/../../../../public/export/'.$name;
            $wExcel->writeToFile($file);
            chmod($file,0777);

            return $this->redirect()->toUrl('http://abonesepeti.org/public/export/'.$name);

            $view = new ViewModel([
                'content' => base64_decode($code)
            ]);
            $view->setTemplate('page/api/csv');
            $this->layout()->setTemplate('layout/csv_layout')->setVariables([
                'title' => date('YmdHis')
            ]);

            return $view;
        }else{
            return $this->redirect()->toRoute('site/user/login');
        }
    }


    public function progressFilterAction()
    {
        $filter = $this->params()->fromRoute('filter');

        $title = 'Hakediş';
        $options = [
            'title' => $title,
            'subtitle' => '',
            'breadcrumb' => [
                [
                    'title' => 'Ana Sayfa',
                    'route' => 'site'
                ],
                [
                    'title' => 'Raporlar',
                    'route' => 'site/report'
                ],
                [
                    'title' => 'Hakediş',
                    'route' => 'site/report/progress'
                ]
            ]
        ];

        $carrierList = $this->carrierCoreTable->carrierList();
        $userList = [];

        foreach ($this->userCoreTable->userList() as $item)
        {
            if($_SESSION['userInfo']['type'] == 'admin')
            {
                if($item['parent_id'] == $_SESSION['userInfo']['id'])
                {
                    $userList[$item['id']] = $item['id'];
                }
            }elseif($_SESSION['userInfo']['type'] == 'user')
            {
                if($item['parent_id'] == $_SESSION['userInfo']['id'])
                {
                    $userList[$item['id']] = $item['id'];
                }
            }
        }

        $carrierListArray = [];

        foreach ($carrierList as $item)
        {
            if($_SESSION['userInfo']['type'] == 'admin')
            {
                $carrierListArray[$item['id']] = $item;
            }elseif($_SESSION['userInfo']['type'] == 'user')
            {
                if(isset($userList[$item['user_id']]))
                {
                    $carrierListArray[$item['id']] = $item;
                }
            }
        }

        $reportList = [];
        foreach ($this->followCoreTable->followBetweebList($filter) as $item)
        {
            if($_SESSION['userInfo']['type'] == 'admin' && $item['status'] == 9)
            {
                $reportList[$carrierListArray[$item['carrier_id']]['name_surname']][$item['id']] = $item['price'];
            }elseif($_SESSION['userInfo']['type'] == 'user'  && $item['status'] == 9)
            {
                if($carrierListArray[$item['carrier_id']])
                {
                    $reportList[$carrierListArray[$item['carrier_id']]['name_surname']][$item['id']] = $item['price'];
                }
            }
        }

        $view = new ViewModel([
            'options' => $options,
            'reportList' => $reportList,
            'carrierList' => $this->carrierCoreTable->carrierList(),
            'status' => $this->status->statusDetailList(),
        ]);
        $view->setTemplate('page/site/report-progress');
        $this->layout()->setTemplate('layout/layout')->setVariables([
            'title' => $title,
            'navbar' => 'progress'
        ]);

        if($_SESSION['loginControl'] == true)
        {
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user/login');
        }
    }
}
