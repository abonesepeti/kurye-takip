<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Follow;

use Campaign\Repository\CampaignTable;
use Carrier\Repository\CarrierCoreTable;
use Cart\Repository\CartTable;
use Core\Repository\CityTable;
use Core\Repository\CountyTable;
use Core\Repository\StatusTable;
use Follow\Repository\FollowCoreTable;
use Follow\Repository\FollowDocsTable;
use Follow\Repository\FollowHistoryTable;
use Magazine\Repository\MagazineCommentsTable;
use Magazine\Repository\MagazineTable;
use Package\Repository\UserSubscriptionsTable;
use User\Controller\SiteController;
use User\Repository\UserAddressTable;
use User\Repository\UserCoreTable;
use User\Repository\UserFavoriteTable;
use User\Repository\UserInfoTable;
use Zend\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'site' => [
                'child_routes' => [
                    'follow' => [
                        'type' => Segment::class,
                        'may_terminate' => true,
                        'options' => [
                            'route'    => 'follow[/]',
                            'defaults' => [
                                'controller' => Controller\SiteController::class,
                                'action'     => 'follow',
                            ],
                        ],
                        'child_routes' => [
                            'check' => [
                                'type' => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'check[/]',
                                    'defaults' => [
                                        'controller' => Controller\SiteController::class,
                                        'action'     => 'check',
                                    ],
                                ],
                            ],
                            'create' => [
                                'type' => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'create[/]',
                                    'defaults' => [
                                        'controller' => Controller\SiteController::class,
                                        'action'     => 'create',
                                    ],
                                ],
                            ],
                            'edit' => [
                                'type' => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'edit/:id[/]',
                                    'defaults' => [
                                        'controller' => Controller\SiteController::class,
                                        'action'     => 'edit',
                                    ],
                                ],
                            ],
                            'upload' => [
                                'type' => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'upload[/]',
                                    'defaults' => [
                                        'controller' => Controller\SiteController::class,
                                        'action'     => 'upload',
                                    ],
                                ],
                            ],
                            'view' => [
                                'type' => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'view/:id[/]',
                                    'defaults' => [
                                        'controller' => Controller\SiteController::class,
                                        'action'     => 'view',
                                    ],
                                ],
                            ],
                        ]
                    ],
                ]
            ],
            'panel' => [
                'child_routes' => [
                    'follow' => [
                        'type'    => Segment::class,
                        'may_terminate' => true,
                        'options' => [
                            'route'    => 'follow[/]',
                            'defaults' => [
                                'controller' => Controller\PanelController::class,
                                'action'     => 'follow',
                            ],
                        ],
                    ],
                ]
            ],
            'api' => [
                'child_routes' => [
                    'follow' => [
                        'type'    => Segment::class,
                        'may_terminate' => true,
                        'options' => [
                            'route'    => 'follow[/]',
                            'defaults' => [
                                'controller' => Controller\APIController::class,
                                'action'     => 'follow',
                            ],
                        ],
                        'child_routes' => [
                            'create' => [
                                'type'    => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'create[/]',
                                    'defaults' => [
                                        'controller' => Controller\APIController::class,
                                        'action'     => 'create',
                                    ],
                                ],
                            ],
                            'edit' => [
                                'type'    => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'edit[/]',
                                    'defaults' => [
                                        'controller' => Controller\APIController::class,
                                        'action'     => 'edit',
                                    ],
                                ],
                            ],
                            'remove' => [
                                'type'    => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'remove[/]',
                                    'defaults' => [
                                        'controller' => Controller\APIController::class,
                                        'action'     => 'remove',
                                    ],
                                ],
                            ],
                            'view' => [
                                'type'    => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'view[/]',
                                    'defaults' => [
                                        'controller' => Controller\APIController::class,
                                        'action'     => 'view',
                                    ],
                                ],
                            ],
                            'historySave' => [
                                'type'    => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'history-save[/]',
                                    'defaults' => [
                                        'controller' => Controller\APIController::class,
                                        'action'     => 'historySave',
                                    ],
                                ],
                            ],
                            'upload' => [
                                'type'    => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'upload[/]',
                                    'defaults' => [
                                        'controller' => Controller\APIController::class,
                                        'action'     => 'upload',
                                    ],
                                ],
                            ],
                            'save' => [
                                'type'    => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'save[/]',
                                    'defaults' => [
                                        'controller' => Controller\APIController::class,
                                        'action'     => 'save',
                                    ],
                                ],
                            ],
                            'pay' => [
                                'type'    => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'pay[/]',
                                    'defaults' => [
                                        'controller' => Controller\APIController::class,
                                        'action'     => 'pay',
                                    ],
                                ],
                            ],
                            'docUpload' => [
                                'type'    => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'doc-upload/:id[/]',
                                    'defaults' => [
                                        'controller' => Controller\APIController::class,
                                        'action'     => 'docUpload',
                                    ],
                                ],
                            ],
                        ]
                    ],
                ]
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\SiteController::class => function($container) {
                return new Controller\SiteController(
                    $container->get(UserCoreTable::class),
                    $container->get(CityTable::class),
                    $container->get(CarrierCoreTable::class),
                    $container->get(FollowCoreTable::class),
                    $container->get(CountyTable::class),
                    $container->get(StatusTable::class),
                    $container->get(FollowHistoryTable::class),
                    $container->get(FollowDocsTable::class)
                );
            },
            Controller\PanelController::class => function($container) {
                return new Controller\PanelController();
            },
            Controller\APIController::class => function($container) {
                return new Controller\APIController(
                    $container->get(UserCoreTable::class),
                    $container->get(FollowCoreTable::class),
                    $container->get(FollowHistoryTable::class),
                    $container->get(CarrierCoreTable::class),
                    $container->get(CityTable::class),
                    $container->get(CountyTable::class),
                    $container->get(FollowDocsTable::class)
                );
            },
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ]
];
