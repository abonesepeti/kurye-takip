<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Follow\Controller;

use Carrier\Repository\CarrierCoreTable;
use Core\Classes\ANKAExcelRead;
use Core\Classes\MethaClasses;
use Core\Repository\CityTable;
use Core\Repository\CountyTable;
use Core\Repository\StatusTable;
use Follow\Model\FollowCore;
use Follow\Model\FollowHistory;
use Follow\Repository\FollowCoreTable;
use Follow\Repository\FollowDocsTable;
use Follow\Repository\FollowHistoryTable;
use User\Repository\UserCoreTable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class SiteController extends AbstractActionController
{
    /** @var $userCoreTable UserCoreTable */
    private $userCoreTable;

    /** @var $cityTable CityTable */
    private $cityTable;

    /** @var $carrierCoreTable CarrierCoreTable */
    private $carrierCoreTable;

    /** @var $followCoreTable FollowCoreTable */
    private $followCoreTable;

    /** @var $county CountyTable */
    private $county;

    /** @var $status StatusTable */
    private $status;

    /** @var $followHistoryTable FollowHistoryTable */
    private $followHistoryTable;

    /** @var $followDocsTable FollowDocsTable */
    private $followDocsTable;

    /** @var $methaClasses MethaClasses */
    private $methaClasses;

    public function __construct()
    {
        $args = func_get_args();
        $this->userCoreTable = $args[0];
        $this->cityTable = $args[1];
        $this->carrierCoreTable = $args[2];
        $this->followCoreTable = $args[3];
        $this->county = $args[4];
        $this->status = $args[5];
        $this->followHistoryTable = $args[6];
        $this->followDocsTable = $args[7];

        $this->methaClasses = new MethaClasses();
    }

    public function followAction()
    {
        $title = 'Evrak Yönetimi';
        $options = [
            'title' => $title,
            'subtitle' => '',
            'breadcrumb' => [
                [
                    'title' => 'Ana Sayfa',
                    'route' => 'site'
                ],
                [
                    'title' => 'Evrak Yönetimi',
                    'route' => 'site/follow'
                ]
            ]
        ];

        $carrierList = $this->carrierCoreTable->carrierList();
        $userList = [];

        foreach ($this->userCoreTable->userList() as $item)
        {
            if($_SESSION['userInfo']['type'] == 'admin')
            {
                $userList[$item['id']] = $item['id'];
            }
        }

        $userCarrierList = [];
        if($_SESSION['userInfo']['type'] == 'user')
        {
            foreach ($carrierList as $items)
            {
                foreach (json_decode($items['dealers']) as $item)
                {
                    print_r($item.PHP_EOL);
                    if($item == 'all' || $item == $_SESSION['userInfo']['id'])
                    {
                        $userCarrierList[$items['user_id']] = $items['user_id'];
                    }
                }
            }
        }

        $followList = [];
        foreach ($this->followCoreTable->followList() as $item)
        {
            $date = new \DateTime($item['create_date']);
            if($_SESSION['userInfo']['type'] == 'admin')
            {
                $followList[] = $item;
            }elseif($_SESSION['userInfo']['type'] == 'user')
            {
                if(isset($userCarrierList[$item['user_id']]))
                {
                    $followList[] = $item;
                }
            }elseif($_SESSION['userInfo']['type'] == 'carrier')
            {
                if($item['user_id'] == $_SESSION['userInfo']['id'])
                {
                    $followList[] = $item;
                }
            }
        }

        $view = new ViewModel([
            'options' => $options,
            'dataList' => $followList,
            'cityList' => $this->cityTable->cityList(),
            'countyList' => $this->county->cityList(),
            'statusList' => $this->status->statusList(),
            'userList' => $this->userCoreTable->userList()
        ]);
        $view->setTemplate('page/site/follow-index');
        $this->layout()->setTemplate('layout/layout')->setVariables([
            'title' => $title,
            'navbar' => 'follow'
        ]);

        $view->setVariables([
            'userInfo' => $this->userCoreTable->findByOneUser([
                'id' => $_SESSION['userInfo']['id']
            ])
        ]);

        if($_SESSION['loginControl'] == true)
        {
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user/login');
        }
    }

    public function checkAction()
    {
        $options = [
            'title' => 'Kurye Takip',
            'subtitle' => 'Kuryenizi, gönderi numarası ile buradan kontrol edebilirsiniz.'
        ];

        $view = new ViewModel([
            'options' => $options
        ]);
        $view->setTemplate('page/site/follow-check');
        $this->layout()->setTemplate('layout/follow_layout')->setVariables([
            'title' => 'Kurye Takip'
        ]);

        $view->setVariables([
            'userInfo' => $this->userCoreTable->findByOneUser([
                'id' => $_SESSION['userInfo']['id']
            ])
        ]);

        if($_SESSION['loginControl'] == true)
        {
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user/login');
        }
    }

    public function createAction()
    {
        $title = 'Evrak Oluştur';
        $options = [
            'title' => $title,
            'subtitle' => 'Gönderinizi bu sayfadan oluşturabilirsiniz.',
            'breadcrumb' => [
                [
                    'title' => 'Ana Sayfa',
                    'route' => 'site'
                ],
                [
                    'title' => 'Evrak Yönetimi',
                    'route' => 'site/follow'
                ],
                [
                    'title' => 'Evrak Oluştur',
                    'route' => 'site/follow/create'
                ]
            ],
            'button' => 'Kaydet',
            'color' => 'primary',
            'icon' => 'fa fa-save',
            'saveURL' => 'api/follow/create'
        ];

        $carrierToUser = $this->userCoreTable->myUsers([
            'parent_id' => $_SESSION['userInfo']['id']
        ]);

        $carrierList = [];
        foreach ($this->carrierCoreTable->carrierList() as $items)
        {
            foreach (json_decode($items['dealers'],true) as $item)
            {
                if($item == 'all' || $item == $_SESSION['userInfo']['id'])
                {
                    $carrierList[] = $items;
                }
            }

            if(isset($carrierToUser[$items['user_id']]))
            {
                // $carrierList[] = $items;
            }
        }

        $view = new ViewModel([
            'options' => $options,
            'cityList' => $this->cityTable->cityList(),
            'carrierList' => $carrierList
        ]);
        $view->setTemplate('page/site/follow-create');
        $this->layout()->setTemplate('layout/layout')->setVariables([
            'title' => 'Evrak Oluştur',
            'navbar' => 'follow'
        ]);

        $view->setVariables([
            'userInfo' => $this->userCoreTable->findByOneUser([
                'id' => $_SESSION['userInfo']['id']
            ])
        ]);

        if($_SESSION['loginControl'] == true)
        {
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user/login');
        }
    }

    public function editAction()
    {
        $id = $this->params()->fromRoute('id');

        $title = 'Evrak Düzenle';
        $options = [
            'title' => $title,
            'subtitle' => 'Gönderinizi bu sayfadan düzenleyebilirsiniz.',
            'breadcrumb' => [
                [
                    'title' => 'Ana Sayfa',
                    'route' => 'site'
                ],
                [
                    'title' => 'Evrak Yönetimi',
                    'route' => 'site/follow'
                ],
                [
                    'title' => 'Evrak Oluştur',
                    'route' => 'site/follow/create'
                ]
            ],
            'button' => 'Düzenle',
            'color' => 'primary',
            'icon' => 'fa fa-edit',
            'saveURL' => 'api/follow/edit'
        ];

        $data = $this->followCoreTable->findByOneFollow([
            'id' => $id
        ]);

        $city = $this->cityTable->titleToKey()[$data['city']];
        $county = $this->county->titleToCounty()[$city][$data['county']];

        $data = array_merge($data,[
            'city' => isset($city) ? $city : $data['city'],
            'county' => isset($county) ? $county : $data['county']
        ]);

        $view = new ViewModel([
            'options' => $options,
            'cityList' => $this->cityTable->cityList(),
            'carrierList' => $this->carrierCoreTable->carrierList(),
            'data' => $data,
            'status' => $this->status->statusList()
        ]);
        $view->setTemplate('page/site/follow-edit');
        $this->layout()->setTemplate('layout/layout')->setVariables([
            'title' => 'Evrak Düzenle',
            'navbar' => 'follow'
        ]);

        $view->setVariables([
            'userInfo' => $this->userCoreTable->findByOneUser([
                'id' => $_SESSION['userInfo']['id']
            ])
        ]);

        if($_SESSION['loginControl'] == true)
        {
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user/login');
        }
    }

    public function uploadAction()
    {
        $title = 'Evrak Yükle';
        $options = [
            'title' => $title,
            'subtitle' => 'Evraklarınızı bu sayfadan yükleyebilirsiniz.',
            'breadcrumb' => [
                [
                    'title' => 'Ana Sayfa',
                    'route' => 'site'
                ],
                [
                    'title' => 'Evrak Yönetimi',
                    'route' => 'site/follow'
                ],
                [
                    'title' => 'Evrak Yükle',
                    'route' => 'site/follow/upload'
                ]
            ],
            'button' => 'Yükle',
            'color' => 'primary',
            'icon' => 'fa fa-upload',
            'saveURL' => 'api/follow/upload'
        ];
        $view = new ViewModel([
            'options' => $options
        ]);

        if ($this->getRequest()->isPost())
        {
            $formFiles = $this->params()->fromFiles();
            $upload = $this->methaClasses->excelUpload($formFiles['file']);

            $dataList = [];
            if(!$upload)
            {
                $excelReader = new ANKAExcelRead('kurye.xlsx');

                foreach ($excelReader->fileRead()['rows'] as $item)
                {
                    $dataList[] = [
                        'code' => strtoupper(hash('crc32',uniqid().date('Y-m-h H:i:s').session_id())),
                        'tc' => $item[1],
                        'user_id' => $_SESSION['userInfo']['id'],
                        'carrier_id' => 0,
                        'name_surname' => $item[2].' '.$item[2],
                        'contact_number' => 0,
                        'address' => 'ADRES',
                        'city' => 0,
                        'county' => 0,
                        'status' => 1,
                        'date' => date('Y-m-d'),
                        'create_date' => date('Y-m-d H:i:s'),
                        'update_date' => date('Y-m-d H:i:s'),
                    ];
                }
            }

            /*
            $query = '';
            foreach ($dataList as $item)
            {
                $sub = '';
                foreach ($item as $key => $value)
                {
                    $sub .= $key.'="'.$value.'" , ';
                }
                $query .= '('.substr($sub,0,-2).'), ';
            }

            $out = substr($query,0,-2);
            $generalQuery = 'INSERT INTO follow_core (code, tc_no, user_id, name_surname, contact_number, address, city, county, status, date, create_date, update_date) VALUES '.$out;
            print_r($generalQuery);
            */

            $resultList = [];

            foreach ($dataList as $item)
            {
                $control = $this->followCoreTable->findByOneFollow([
                    'tc_no' => $item['tc_no']
                ]);

                if(count($control) > 0)
                {

                }else{
                    $item = array_merge($item,[
                        'carrier_id' => $item['carrier_id'] == null ? '1' : $item['carrier_id']
                    ]);
                    $followCore = new FollowCore();
                    $followCore->exchangeArray($item);

                    $result = $this->followCoreTable->saveFollow($followCore);
                    if($result)
                    {
                        $resultList[] = [
                            'code' => 200
                        ];
                    }else{
                        $resultList[] = [
                            'code' => 201,
                            'message' => $result
                        ];
                    }
                }
            }

            print_r($resultList);
            die;
        }

        $view->setTemplate('page/site/follow-upload');
        $this->layout()->setTemplate('layout/layout')->setVariables([
            'title' => 'Evrak Yükle',
            'navbar' => 'follow'
        ]);

        $view->setVariables([
            'userInfo' => $this->userCoreTable->findByOneUser([
                'id' => $_SESSION['userInfo']['id']
            ])
        ]);

        if($_SESSION['loginControl'] == true)
        {
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user/login');
        }
    }

    public function viewAction()
    {
        $id = $this->params()->fromRoute('id');
        $title = 'Evrak İncele';
        $options = [
            'title' => $title,
            'subtitle' => 'Gönderinizi bu sayfadan oluşturabilirsiniz.',
            'breadcrumb' => [
                [
                    'title' => 'Ana Sayfa',
                    'route' => 'site'
                ],
                [
                    'title' => 'Evrak Yönetimi',
                    'route' => 'site/follow'
                ],
                [
                    'title' => 'Evrak İncele',
                    'route' => 'site/follow'
                ]
            ],
            'button' => 'Düzenle',
            'color' => 'primary',
            'icon' => 'fa fa-edit',
            'saveURL' => 'api/follow/edit'
        ];
        $view = new ViewModel([
            'options' => $options,
            'cityList' => $this->cityTable->cityList(),
            'carrierList' => $this->carrierCoreTable->carrierList(),
            'data' => $this->followCoreTable->findByOneFollow([
                'id' => $id
            ]),
            'status' => $this->status->statusList(),
            'docs' => $this->followDocsTable->followDocs([
                'follow_id' => $id
            ])
        ]);
        $view->setTemplate('page/site/follow-view');
        $this->layout()->setTemplate('layout/layout')->setVariables([
            'title' => 'Evrak İncele',
            'navbar' => 'follow'
        ]);

        if ($this->getRequest()->isPost())
        {
            $formData = $this->params()->fromPost();

            $formData = array_merge($formData,[
                'follow_id' => $id,
                'create_date' => date('Y-m-d H:i:s'),
            ]);

            $updateResult = $this->followCoreTable->updateFollow([
                'status' => $formData['status'],
                'update_date' => date('Y-m-d H:i:s')
            ],[
                'id' => $id
            ]);

            $formData = array_merge($formData,[
                'logs' => json_encode([
                    'description' => $formData['description'],
                    'address' => $formData['address'],
                    'user_id' => $_SESSION['userInfo']['id'],
                    'status' => $formData['status'],
                    'id' => $id
                ])
            ]);
            $followHistory = new FollowHistory();
            $followHistory->exchangeArray($formData);

            $result = $this->followHistoryTable->saveFollow($followHistory);
            if($result)
            {
                return $this->redirect()->toRoute('site');
            }
        }

        $view->setVariables([
            'userInfo' => $this->userCoreTable->findByOneUser([
                'id' => $_SESSION['userInfo']['id']
            ])
        ]);

        if($_SESSION['loginControl'] == true)
        {
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user/login');
        }
    }
}
