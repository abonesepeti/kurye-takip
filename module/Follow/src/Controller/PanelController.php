<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Follow\Controller;

use Zend\Mvc\Controller\AbstractActionController;

class PanelController extends AbstractActionController
{
    public function __construct()
    {
        $args = func_get_args();
    }

    public function followAction()
    {
        die('FOLLOW');
    }
}
