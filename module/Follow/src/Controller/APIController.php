<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Follow\Controller;

use Carrier\Repository\CarrierCoreTable;
use Core\Classes\ANKAExcelRead;
use Core\Classes\MethaClasses;
use Core\Repository\CityTable;
use Core\Repository\CountyTable;
use Follow\Model\FollowCore;
use Follow\Model\FollowDocs;
use Follow\Model\FollowHistory;
use Follow\Module;
use Follow\Repository\FollowCoreTable;
use Follow\Repository\FollowDocsTable;
use Follow\Repository\FollowHistoryTable;
use User\Repository\UserCoreTable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class APIController extends AbstractActionController
{
    /** @var $userCoreTable UserCoreTable */
    private $userCoreTable;

    /** @var $followCoreTable FollowCoreTable */
    private $followCoreTable;

    /** @var $followHistoryTable FollowHistoryTable */
    private $followHistoryTable;

    /** @var $carrierCoreTable CarrierCoreTable */
    private $carrierCoreTable;

    /** @var $cityTable CityTable */
    private $cityTable;

    /** @var $countyTable CountyTable */
    private $countyTable;

    /** @var $followDocsTable FollowDocsTable */
    private $followDocsTable;

    /** @var $methaClasses MethaClasses */
    private $methaClasses;

    public function __construct()
    {
        $args = func_get_args();
        $this->userCoreTable = $args[0];
        $this->followCoreTable = $args[1];
        $this->followHistoryTable = $args[2];
        $this->carrierCoreTable = $args[3];
        $this->cityTable = $args[4];
        $this->countyTable = $args[5];
        $this->followDocsTable = $args[6];

        $this->methaClasses = new MethaClasses();
    }

    public function followAction()
    {
        $dataList = $this->followCoreTable->followList();

        $view = new ViewModel([
            'content' => $dataList
        ]);
        $view->setTemplate('page/site/json');
        $this->layout()->setTemplate('layout/json_layout');

        return $view;
    }

    public function createAction()
    {
        $view = new ViewModel();

        if ($this->getRequest()->isPost())
        {
            $formData = $this->params()->fromPost();

            $carrierDetail = $this->carrierCoreTable->findByOneCarrier([
                'id' => $formData['carrier_id']
            ]);

            $prices = json_decode($carrierDetail['price'],true);

            $formData = array_merge($formData,[
                'user_id' => $carrierDetail['user_id'],
                'create_date' => date('Y-m-d H:i:s'),
                'update_date' => date('Y-m-d H:i:s'),
                'status' => 1,
                'code' => strtoupper(hash('crc32',uniqid().date('Y-m-h H:i:s').session_id())),
                'maya_id' => isset($formData['maya_id']) ? $formData['maya_id'] : 0,
                'tc_no' => isset($formData['tc_no']) ? $formData['tc_no'] : 1,
                'date' => isset($formData['date']) ? $formData['date'] : date('Y-m-d'),
                'price' => isset($prices['aktif']) ? $prices['aktif'] : $prices['gonderi'],
                'add_user' => $_SESSION['userInfo']['id'],
                'city' => isset($formData['city']) ? $formData['city'] : 0,
                'county' => isset($formData['county']) ? $formData['county'] : 0,
                'package' => $formData['package'],
                'number' => $formData['number'],
                'description' => $formData['description'],
                'contact_number' => json_encode([$formData['contact_number']])
            ]);

            if($formData['name_surname'] == '')
            {
                $view->setVariables([
                    'content' => [
                        'code' => 201,
                        'message' => 'Müşteri adı soyadı boş bırakılamaz.',
                        'post_name' => 'name_surname'
                    ]
                ]);
            }elseif($formData['carrier_id'] == '')
            {
                $view->setVariables([
                    'content' => [
                        'code' => 201,
                        'message' => 'Lütfen kurye seçimi yapını.',
                        'post_name' => 'carrier_id'
                    ]
                ]);
            }elseif($formData['address'] == '')
            {
                $view->setVariables([
                    'content' => [
                        'code' => 201,
                        'message' => 'Lütfen evrağın teslim edileceği adresi yazınız.',
                        'post_name' => 'address'
                    ]
                ]);
            }elseif($formData['city'] == '')
            {
                $view->setVariables([
                    'content' => [
                        'code' => 201,
                        'message' => 'Evrağın teslim edileceği şehri giriniz.',
                        'post_name' => 'city'
                    ]
                ]);
            }elseif($formData['county'] == '')
            {
                $view->setVariables([
                    'content' => [
                        'code' => 201,
                        'message' => 'Evrağın teslim edileceği ilçeyi giriniz.',
                        'post_name' => 'county'
                    ]
                ]);
            }else{
                $followCore = new FollowCore();
                $followCore->exchangeArray($formData);

                $result = $this->followCoreTable->saveFollow($followCore);

                if($result)
                {
                    $view->setVariables([
                        'content' => [
                            'code' => 200,
                            'message' => 'Evrak ekleme işleminiz başarılı bir şekilde gerçekleştirilmiştir.'
                        ]
                    ]);
                }else{
                    $view->setVariables([
                        'content' => [
                            'code' => 201,
                            'message' => 'Evrak ekleme işleminiz sırasında bir hata ile karşışaltık.Lütfen daha sonra tekrar deneyiniz.'
                        ]
                    ]);
                }
            }
        }else{
            $view->setVariables([
                'content' => [
                    'code' => 403,
                    'message' => 'Yetkisiz erişim.'
                ]
            ]);
        }

        $view->setTemplate('page/site/json');
        $this->layout()->setTemplate('layout/json_layout');

        return $view;
    }

    public function editAction()
    {
        $view = new ViewModel();

        if ($this->getRequest()->isPost())
        {
            $formData = $this->params()->fromPost();

            if($formData['type'] == 'view')
            {
                $result = $this->followCoreTable->updateFollow([
                    'status' => $formData['status'],
                    'description' => $formData['description'],
                    'update_date' => date('Y-m-d H:i:s')
                ],$formData['id']);
            }else{
                $result = $this->followCoreTable->updateFollow([
                    'name_surname' => $formData['name_surname'],
                    'carrier_id' => $formData['carrier_id'],
                    'tc_no' => $formData['tc_no'],
                    'date' => $formData['date'],
                    'contact_number' => json_encode([$formData['contact_number']]),
                    'address' => $formData['address'],
                    'update_date' => date('Y-m-d H:i:s'),
                    'city' => $formData['city'],
                    'county' => $formData['county'],
                    'status' => $formData['status'],
                    'package' => $formData['package'],
                    'number' => $formData['number'],
                    'pay' => $formData['pay']
                ],$formData['id']);
            }

            if($result)
            {
                $view->setVariables([
                    'content' => [
                        'code' => 200,
                        'message' => 'Evrak düzenleme işleminiz başarılı bir şekilde gerçekleştirilmiştir.'
                    ]
                ]);
            }else{
                $view->setVariables([
                    'content' => [
                        'code' => 201,
                        'message' => 'Evrak düzenleme işleminiz sırasında bir hata ile karşışaltık.Lütfen daha sonra tekrar deneyiniz.'
                    ]
                ]);
            }
        }else{
            $view->setVariables([
                'content' => [
                    'code' => 403,
                    'message' => 'Yetkisiz erişim.'
                ]
            ]);
        }

        $view->setTemplate('page/site/json');
        $this->layout()->setTemplate('layout/json_layout');

        if($_SESSION['loginControl'] == true)
        {
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user/login');
        }
    }

    public function removeAction()
    {
        $view = new ViewModel();

        if ($this->getRequest()->isPost())
        {
            $formData = $this->params()->fromPost();

            if($_SESSION['userInfo']['type'] != 'carrier')
            {
                $result = $this->followCoreTable->removeFollow($formData['id']);

                if($result)
                {
                    $view->setVariables([
                        'content' => [
                            'code' => 200,
                            'message' => 'Evrak silme işleminiz başarılı bir şekilde gerçekleştirilmiştir.'
                        ]
                    ]);
                }else{
                    $view->setVariables([
                        'content' => [
                            'code' => 201,
                            'message' => 'Evrak silme işleminiz sırasında bir hata ile karşışaltık.Lütfen daha sonra tekrar deneyiniz.'
                        ]
                    ]);
                }
            }else{
                $view->setVariables([
                    'content' => [
                        'code' => 403,
                        'message' => 'Bu veriyi silmeye yetkiniz bulunmamaktadır.'
                    ]
                ]);
            }
        }else{
            $view->setVariables([
                'content' => [
                    'code' => 403,
                    'message' => 'Yetkisiz erişim.'
                ]
            ]);
        }

        $view->setTemplate('page/site/json');
        $this->layout()->setTemplate('layout/json_layout');

        if($_SESSION['loginControl'] == true)
        {
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user/login');
        }
    }

    public function viewAction()
    {
        $view = new ViewModel();

        if ($this->getRequest()->isPost())
        {
            $formData = $this->params()->fromPost();
            $formData = array_merge($formData,[
                'create_date' => date('Y-m-d H:i:s'),
                'logs' => json_encode([
                    'address' => $formData['address']
                ])
            ]);

            $followCore = new FollowHistory();
            $followCore->exchangeArray($formData);
            $result = $this->followHistoryTable->saveFollow($followCore);

            if($result)
            {
                $view->setVariables([
                    'content' => [
                        'code' => 200,
                        'message' => 'İşleminiz başarılı bir şekilde gerçekleştirilmiştir.'
                    ]
                ]);
            }else{
                $view->setVariables([
                    'content' => [
                        'code' => 201,
                        'message' => 'İşlem sırasında bir hata ile karşılaştık.Lütfen daha sonra tekrar deneyiniz.'
                    ]
                ]);
            }
        }else{
            $view->setVariables([
                'content' => [
                    'code' => 403,
                    'message' => 'Yetkisiz erişim.'
                ]
            ]);
        }

        $view->setTemplate('page/site/json');
        $this->layout()->setTemplate('layout/json_layout');

        if($_SESSION['loginControl'] == true)
        {
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user/login');
        }
    }

    public function historySaveAction()
    {
        $view = new ViewModel();

        if ($this->getRequest()->isPost())
        {
            $formData = $this->params()->fromPost();
            $formFiles = $this->params()->fromFiles();

            $formData = array_merge($formData,[
                'follow_id' => $formData['id'],
                'create_date' => date('Y-m-d H:i:s'),
            ]);

            $updateResult = $this->followCoreTable->updateFollow([
                'status' => $formData['status'],
                'update_date' => date('Y-m-d H:i:s')
            ],[
                'id' => $formData['id']
            ]);

            if($updateResult)
            {
                $docResult = $this->methaClasses->documentUpload($formFiles['file']);

                $formData = array_merge($formData,[
                    'logs' => json_encode([
                        'description' => $formData['description'],
                        'status' => $formData['status'],
                        'id' => $formData['id'],
                        'doc' => $docResult
                    ])
                ]);
                $followHistory = new FollowHistory();
                $followHistory->exchangeArray($formData);

                $result = $this->followHistoryTable->saveFollow($followHistory);
                if($result)
                {
                    return $this->redirect()->toRoute('site');
                }
            }
        }else{
            $view->setVariables([
                'content' => [
                    'code' => 403,
                    'message' => 'Yetkisiz erişim.'
                ]
            ]);
        }

        $view->setTemplate('page/site/json');
        $this->layout()->setTemplate('layout/json_layout');

        return $view;
    }

    public function uploadAction()
    {
        $view = new ViewModel();

        if ($this->getRequest()->isPost())
        {
            $formData = $this->params()->fromPost();
            $formFiles = $this->params()->fromFiles();

            $formData = array_merge($formData,[
                'follow_id' => $formData['id'],
                'create_date' => date('Y-m-d H:i:s'),
            ]);

            $upload = $this->methaClasses->excelUpload($formFiles['integrationFile']);
            $excelReader = new ANKAExcelRead(Module::FOLLOW_DOCUMENT_ROUTE.$upload);

            $columnsList = [];
            foreach ($excelReader->fileRead()['columns'] as $item)
            {
                $columnsList[] = $item;
            }

            $view->setVariables([
                'content' => [
                    'code' => 200,
                    'file' => $upload,
                    'result' => $columnsList
                ]
            ]);
        }else{
            $view->setVariables([
                'content' => [
                    'code' => 403,
                    'message' => 'Yetkisiz erişim.'
                ]
            ]);
        }

        $view->setTemplate('page/site/json');
        $this->layout()->setTemplate('layout/json_layout');

        return $view;
    }

    public function saveAction()
    {
        $view = new ViewModel();

        if ($this->getRequest()->isPost())
        {
            $formData = $this->params()->fromPost();
            $formFiles = $this->params()->fromFiles();

            $cityList = $this->cityTable->titleToKey();
            $countyList = $this->countyTable->titleToCounty();

            /**
             * todo: İl ve İlçelere göre kuryeleri listeliyoruz.
             */
            $carrierList = [];
            foreach ($this->carrierCoreTable->carrierList() as $item)
            {
                $areaList = [];
                foreach (json_decode($item['area'],true) as $key => $area)
                {
                    foreach ($area as $id => $area_)
                    {
                        $carrierList[$area_] = [
                            'carrier_id' => $item['id'],
                            'user_id' => $item['user_id'],
                            'price' => json_decode($item['price'],true)['aktif']
                        ];
                    }
                }
            }

            $formData = array_merge($formData,[
                'follow_id' => $formData['id'],
                'create_date' => date('Y-m-d H:i:s'),
            ]);

            $columns = [];
            foreach ($formData['column'] as $key => $item)
            {
                if($item != "0")
                {
                    $columns[$key] = $item;
                }
            }

            $file = Module::FOLLOW_DOCUMENT_ROUTE.$formData['fileName'];
            $excelReader = new ANKAExcelRead($file);

            $resultList = [];
            foreach ($excelReader->fileRead()['rows'] as $items)
            {
                $newData = [];
                foreach ($items as $key => $value)
                {
                    if(isset($columns[$key]))
                    {
                        $newData[$columns[$key]] = $value;
                    }
                }

                /**
                 * todo: user_id alanumız, carrier_id kısmında oluşturulan kuryenin user_id'si olacak.
                 */
                $myData = array_merge($newData,[
                    'name_surname' => $newData['name'].' '.$newData['surname'],
                    'contact_number' => $newData['gsm'],
                    'create_date' => date('Y-m-d H:i:s'),
                    'update_date' => date('Y-m-d H:i:s'),
                    'status' => 1,
                    'code' => strtoupper(hash('crc32',uniqid().date('Y-m-h H:i:s').session_id())),
                    'data_type' => null,
                    'city' => $cityList[$newData['city']],
                    'county' => $countyList[$cityList[$newData['city']]][$newData['county']],
                    'carrier_id' => $carrierList[$countyList[$cityList[$newData['city']]][$newData['county']]]['carrier_id'],
                    'user_id' => $carrierList[$countyList[$cityList[$newData['city']]][$newData['county']]]['user_id'],
                    'price' => $carrierList[$countyList[$cityList[$newData['city']]][$newData['county']]]['price'],
                    'add_user' => $_SESSION['userInfo']['id']
                ]);
                $followCore = new FollowCore();
                $followCore->exchangeArray($myData);

                $result = $this->followCoreTable->saveFollow($followCore);
                if($result)
                {
                    $resultList[200][] = $result;
                }else{
                    $resultList[201][] = [
                        'name_surname' => $myData['name_surname'],
                        'message' => json_encode($result)
                    ];
                }
            }

            $view->setVariables([
                'content' => [
                    'code' => 200,
                    'message' => 'Yetkisiz erişim.',
                    'result' => $resultList
                ]
            ]);
        }else{
            $view->setVariables([
                'content' => [
                    'code' => 403,
                    'message' => 'Yetkisiz erişim.'
                ]
            ]);
        }

        $view->setTemplate('page/site/json');
        $this->layout()->setTemplate('layout/json_layout');

        return $view;
    }

    public function docUploadAction()
    {
        $id = $this->params()->fromRoute('id');

        $view = new ViewModel();

        if ($this->getRequest()->isPost())
        {
            $formFiles = $this->params()->fromFiles();

            $fileUpload = $this->methaClasses->documentUpload($formFiles['file']);

            if($fileUpload)
            {
                $followDocs = new FollowDocs();
                $followDocs->exchangeArray([
                    'follow_id' => $id,
                    'docs' => $fileUpload,
                    'create_date' => date('Y-m-d H:i:s')
                ]);

                $result = $this->followDocsTable->saveFollowDocs($followDocs);
                if($result)
                {
                    $view->setVariables([
                        'content' => [
                            'code' => 200,
                            'message' => 'Evrak yükleme işleminiz başarılı bir şekilde gerçekleştirilmiştir.'
                        ]
                    ]);
                }else{
                    $view->setVariables([
                        'content' => [
                            'code' => 201,
                            'message' => 'Evrak kayıt sırasında bir hata ile karşılaştık.Lütfen daha sonra tekrar deneyiniz.'
                        ]
                    ]);
                }
            }else{
                $view->setVariables([
                    'content' => [
                        'code' => 201,
                        'message' => 'Evrak yükleme sırasında bir hata ile karşılaştık.Lütfen daha sonra tekrar deneyiniz.'
                    ]
                ]);
            }
        }else{
            $view->setVariables([
                'content' => [
                    'code' => 403,
                    'message' => 'Yetkisiz erişim.'
                ]
            ]);
        }

        $view->setTemplate('page/site/json');
        $this->layout()->setTemplate('layout/json_layout');

        return $view;
    }
}
