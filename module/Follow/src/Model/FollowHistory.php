<?php
/**
 * Time: 22:49
 * Description:
 */

namespace Follow\Model;

class FollowHistory
{
    public $id;
    public $followID;
    public $logs;
    public $status;
    public $createDate;

    public function exchangeArray($data)
    {
        $this->id  = (!empty($data['id'])) ? $data['id'] : null;
        $this->followID = (!empty($data['follow_id'])) ? $data['follow_id'] : null;
        $this->logs = (!empty($data['logs'])) ? $data['logs'] : null;
        $this->status = (!empty($data['status'])) ? $data['status'] : null;
        $this->createDate = (!empty($data['create_date'])) ? $data['create_date'] : null;
    }

    public function getArrayCopy()
    {
        return [
            'id' => $this->id,
            'follow_id' => $this->followID,
            'logs' => $this->logs,
            'status' => $this->status,
            'create_date' => $this->createDate
        ];
    }
}
