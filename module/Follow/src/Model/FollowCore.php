<?php
/**
 * Time: 22:49
 * Description:
 */

namespace Follow\Model;

class FollowCore
{
    public $id;
    public $code;
    public $mayaID;
    public $tcNo;
    public $userID;
    public $carrierID;
    public $nameSurname;
    public $contactNumber;
    public $address;
    public $city;
    public $county;
    public $status;
    public $date;
    public $price;
    public $pay;
    public $package;
    public $number;
    public $dataType;
    public $addUser;
    public $description;
    public $createDate;
    public $updateDate;

    public function exchangeArray($data)
    {
        $this->id  = (!empty($data['id'])) ? $data['id'] : null;
        $this->code = (!empty($data['code'])) ? $data['code'] : null;
        $this->mayaID = (!empty($data['maya_id'])) ? $data['maya_id'] : null;
        $this->tcNo = (!empty($data['tc_no'])) ? $data['tc_no'] : null;
        $this->userID = (!empty($data['user_id'])) ? $data['user_id'] : null;
        $this->carrierID = (!empty($data['carrier_id'])) ? $data['carrier_id'] : null;
        $this->nameSurname = (!empty($data['name_surname'])) ? $data['name_surname'] : null;
        $this->contactNumber = (!empty($data['contact_number'])) ? $data['contact_number'] : null;
        $this->address = (!empty($data['address'])) ? $data['address'] : null;
        $this->city = (!empty($data['city'])) ? $data['city'] : null;
        $this->county = (!empty($data['county'])) ? $data['county'] : null;
        $this->status = (!empty($data['status'])) ? $data['status'] : null;
        $this->date = (!empty($data['date'])) ? $data['date'] : null;
        $this->price = (!empty($data['price'])) ? $data['price'] : null;
        $this->pay = (!empty($data['pay'])) ? $data['pay'] : null;
        $this->package = (!empty($data['package'])) ? $data['package'] : null;
        $this->number = (!empty($data['number'])) ? $data['number'] : null;
        $this->dataType = (!empty($data['data_type'])) ? $data['data_type'] : null;
        $this->addUser = (!empty($data['add_user'])) ? $data['add_user'] : null;
        $this->description = (!empty($data['description'])) ? $data['description'] : null;
        $this->createDate = (!empty($data['create_date'])) ? $data['create_date'] : null;
        $this->updateDate = (!empty($data['update_date'])) ? $data['update_date'] : null;
    }

    public function getArrayCopy()
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'maya_id' => $this->mayaID,
            'tc_no' => $this->tcNo,
            'user_id' => $this->userID,
            'carrier_id' => $this->carrierID,
            'name_surname' => $this->nameSurname,
            'contact_number' => $this->contactNumber,
            'address' => $this->address,
            'city' => $this->city,
            'county' => $this->county,
            'status' => $this->status,
            'date' => $this->date,
            'price' => $this->price,
            'pay' => $this->pay,
            'package' => $this->package,
            'number' => $this->number,
            'data_type' => $this->dataType,
            'add_user' => $this->addUser,
            'description' => $this->description,
            'create_date' => $this->createDate,
            'update_date' => $this->updateDate
        ];
    }
}
