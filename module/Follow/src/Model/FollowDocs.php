<?php
/**
 * Time: 22:49
 * Description:
 */

namespace Follow\Model;

class FollowDocs
{
    public $id;
    public $followID;
    public $docs;
    public $createDate;

    public function exchangeArray($data)
    {
        $this->id  = (!empty($data['id'])) ? $data['id'] : null;
        $this->followID = (!empty($data['follow_id'])) ? $data['follow_id'] : null;
        $this->docs = (!empty($data['docs'])) ? $data['docs'] : null;
        $this->createDate = (!empty($data['create_date'])) ? $data['create_date'] : null;
    }

    public function getArrayCopy()
    {
        return [
            'id' => $this->id,
            'follow_id' => $this->followID,
            'docs' => $this->docs,
            'create_date' => $this->createDate
        ];
    }
}
