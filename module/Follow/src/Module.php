<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Follow;

use Follow\Model\FollowCore;
use Follow\Model\FollowDocs;
use Follow\Model\FollowHistory;
use Follow\Repository\FollowCoreTable;
use Follow\Repository\FollowDocsTable;
use Follow\Repository\FollowHistoryTable;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ModuleManager\Feature\ConfigProviderInterface;

class Module implements ConfigProviderInterface
{
    //const FOLLOW_DOCUMENT_ROUTE = '/home/absorg/public_html/public/upload/';
    const FOLLOW_DOCUMENT_ROUTE = '/opt/lampp/htdocs/kurye-takip/public/upload/';

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                FollowCoreTable::class => function ($container) {
                    $tableGateway = $container->get('Repository\FollowCoreTableGateway');
                    $table = new FollowCoreTable($tableGateway);
                    return $table;
                },
                'Repository\FollowCoreTableGateway' => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new FollowCore());
                    return new TableGateway('follow_core', $dbAdapter, null, $resultSetPrototype);
                },
                FollowHistoryTable::class => function ($container) {
                    $tableGateway = $container->get('Repository\FollowHistoryTableGateway');
                    $table = new FollowHistoryTable($tableGateway);
                    return $table;
                },
                'Repository\FollowHistoryTableGateway' => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new FollowHistory());
                    return new TableGateway('follow_history', $dbAdapter, null, $resultSetPrototype);
                },
                FollowDocsTable::class => function ($container) {
                    $tableGateway = $container->get('Repository\FollowDocsTableGateway');
                    $table = new FollowDocsTable($tableGateway);
                    return $table;
                },
                'Repository\FollowDocsTableGateway' => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new FollowDocs());
                    return new TableGateway('follow_docs', $dbAdapter, null, $resultSetPrototype);
                },
            ],
        ];
    }
}
