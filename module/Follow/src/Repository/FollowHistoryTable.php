<?php
/**
 * Time: 13:22
 */
namespace Follow\Repository;

use Exception;
use Follow\Model\FollowCore;
use Follow\Model\FollowHistory;
use Zend\Db\TableGateway\TableGateway;

class FollowHistoryTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function followList()
    {
        $dataList = [];
        $rowset = $this->tableGateway->select();

        foreach ($rowset as $item)
        {
            $dataList[] = [
                'id' => $item->id,
                'follow_id' => $item->followID,
                'logs' => $item->logs,
                'status' => $item->status,
                'create_date' => $item->createDate
            ];
        }

        return $dataList;
    }

    public function statusToFollowList()
    {
        $dataList = [];
        $rowset = $this->tableGateway->select();

        foreach ($rowset as $item)
        {
            $dataList[$item->status][] = [
                'id' => $item->id,
                'follow_id' => $item->followID,
                'logs' => $item->logs,
                'status' => $item->status,
                'create_date' => $item->createDate
            ];
        }

        return $dataList;
    }

    public function findByOneFollow(array $data)
    {
        $dataList = [];
        $rowset = $this->tableGateway->select($data);

        foreach ($rowset as $item)
        {
            $dataList = [
                'id' => $item->id,
                'follow_id' => $item->followID,
                'logs' => $item->logs,
                'status' => $item->status,
                'create_date' => $item->createDate
            ];
        }

        return $dataList;
    }

    public function saveFollow(FollowHistory $item)
    {
        $data = [
            'follow_id' => $item->followID,
            'logs' => $item->logs,
            'status' => $item->status,
            'create_date' => $item->createDate
        ];

        try{
            $result =  $this->tableGateway->insert($data);
            if($result)
            {
                return $this->tableGateway->getLastInsertValue();
            }
        }catch (Exception $e)
        {
            return $e->getMessage();
        }
    }

    public function removeFollow($id)
    {
        $result = $this->tableGateway->delete(['id' => $id]);
        if($result)
        {
            return true;
        }else{
            return false;
        }
    }

    public function updateFollow(array $data,$id)
    {
        $result = $this->tableGateway->update($data, ['id' => $id]);
        if($result)
        {
            return true;
        }else{
            return false;
        }
    }
}
