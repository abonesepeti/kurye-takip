<?php
/**
 * Time: 13:22
 */
namespace Follow\Repository;

use Exception;
use Follow\Model\FollowCore;
use Zend\Db\TableGateway\TableGateway;

class FollowCoreTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function followList()
    {
        $dataList = [];
        $rowset = $this->tableGateway->select();

        foreach ($rowset as $item)
        {
            $dataList[] = [
                'id' => $item->id,
                'maya_id' => $item->mayaID,
                'code' => $item->code,
                'tc_no' => $item->tcNo,
                'user_id' => $item->userID,
                'carrier_id' => $item->carrierID,
                'name_surname' => $item->nameSurname,
                'contact_number' => $item->contactNumber,
                'address' => $item->address,
                'city' => $item->city,
                'county' => $item->county,
                'status' => $item->status,
                'date' => $item->date,
                'price' => $item->price,
                'pay' => $item->pay,
                'package' => $item->package,
                'number' => $item->number,
                'data_type' => $item->dataType,
                'add_user' => $item->addUser,
                'description' => $item->description,
                'create_date' => $item->createDate,
                'update_date' => $item->updateDate
            ];
        }

        return $dataList;
    }

    public function followBetweebList(string $dates)
    {
        $dates = explode('&',$dates);

        $filter = [];
        $datas = [];

        foreach ($dates as $item)
        {
            $exp = explode('=',$item);

            if($exp[0] == 'start_date' || $exp[0] == 'end_date')
            {
                $datas[$exp[0]] = $exp[1];
            }else{
                if($exp[1] != 0)
                {
                    $filter[$exp[0]] = $exp[1];
                }
            }
        }

        $dateList = [];

        $Date1 = $datas['start_date'];
        $Date2 = $datas['end_date'];

        $array = array();

        $Variable1 = strtotime($Date1);
        $Variable2 = strtotime($Date2);

        for ($currentDate = $Variable1; $currentDate <= $Variable2;
             $currentDate += (86400)) {

            $Store = date('Y-m-d', $currentDate);
            $dateList[$Store] = $Store;
        }

        $dataList = [];

        $rowset = $this->tableGateway->select($filter);

        foreach ($rowset as $item)
        {
            $date = new \DateTime($item->createDate);
            if(isset($dateList[$date->format('Y-m-d')]))
            {
                $dataList[] = [
                    'id' => $item->id,
                    'maya_id' => $item->mayaID,
                    'code' => $item->code,
                    'tc_no' => $item->tcNo,
                    'user_id' => $item->userID,
                    'carrier_id' => $item->carrierID,
                    'name_surname' => $item->nameSurname,
                    'contact_number' => $item->contactNumber,
                    'address' => $item->address,
                    'city' => $item->city,
                    'county' => $item->county,
                    'status' => $item->status,
                    'date' => $item->date,
                    'price' => $item->price,
                    'pay' => $item->pay,
                    'package' => $item->package,
                    'number' => $item->number,
                    'data_type' => $item->dataType,
                    'add_user' => $item->addUser,
                    'description' => $item->description,
                    'create_date' => $item->createDate,
                    'update_date' => $item->updateDate
                ];
            }
        }

        return $dataList;
    }

    public function statusToFollowList()
    {
        $dataList = [];
        $rowset = $this->tableGateway->select();

        foreach ($rowset as $item)
        {
            $dataList[$item->status][] = [
                'id' => $item->id,
                'maya_id' => $item->mayaID,
                'code' => $item->code,
                'tc_no' => $item->tcNo,
                'user_id' => $item->userID,
                'carrier_id' => $item->carrierID,
                'name_surname' => $item->nameSurname,
                'contact_number' => $item->contactNumber,
                'address' => $item->address,
                'city' => $item->city,
                'county' => $item->county,
                'status' => $item->status,
                'date' => $item->date,
                'price' => $item->price,
                'pay' => $item->pay,
                'package' => $item->package,
                'number' => $item->number,
                'data_type' => $item->dataType,
                'add_user' => $item->addUser,
                'description' => $item->description,
                'create_date' => $item->createDate,
                'update_date' => $item->updateDate
            ];
        }

        return $dataList;
    }

    public function findByOneFollow(array $data)
    {
        $dataList = [];
        $rowset = $this->tableGateway->select($data);

        foreach ($rowset as $item)
        {
            $dataList = [
                'id' => $item->id,
                'maya_id' => $item->mayaID,
                'code' => $item->code,
                'tc_no' => $item->tcNo,
                'user_id' => $item->userID,
                'carrier_id' => $item->carrierID,
                'name_surname' => $item->nameSurname,
                'contact_number' => $item->contactNumber,
                'address' => $item->address,
                'city' => $item->city,
                'county' => $item->county,
                'status' => $item->status,
                'date' => $item->date,
                'price' => $item->price,
                'pay' => $item->pay,
                'package' => $item->package,
                'number' => $item->number,
                'data_type' => $item->dataType,
                'add_user' => $item->addUser,
                'description' => $item->description,
                'create_date' => $item->createDate,
                'update_date' => $item->updateDate
            ];
        }

        return $dataList;
    }

    public function saveFollow(FollowCore $item)
    {
        $data = [
            'maya_id' => $item->mayaID,
            'code' => $item->code,
            'tc_no' => $item->tcNo,
            'user_id' => $item->userID,
            'carrier_id' => $item->carrierID,
            'name_surname' => $item->nameSurname,
            'contact_number' => $item->contactNumber,
            'address' => $item->address,
            'city' => $item->city,
            'county' => $item->county,
            'status' => $item->status,
            'price' => $item->price,
            'pay' => $item->pay,
            'package' => $item->package,
            'number' => $item->number,
            'data_type' => $item->dataType,
            'add_user' => $item->addUser,
            'description' => $item->description,
            'date' => $item->date,
            'create_date' => $item->createDate,
            'update_date' => $item->updateDate
        ];

        try{
            $result =  $this->tableGateway->insert($data);
            if($result)
            {
                return true;
            }else{
                return $result;
            }
        }catch (Exception $e)
        {
            return $e->getMessage();
        }
    }

    public function removeFollow($id)
    {
        $result = $this->tableGateway->delete(['id' => $id]);
        if($result)
        {
            return true;
        }else{
            return false;
        }
    }

    public function updateFollow(array $data,$id)
    {
        $result = $this->tableGateway->update($data, ['id' => $id]);
        if($result)
        {
            return true;
        }else{
            return false;
        }
    }
}
