<?php
/**
 * Time: 13:22
 */
namespace Follow\Repository;

use Exception;
use Follow\Model\FollowCore;
use Follow\Model\FollowDocs;
use Follow\Model\FollowHistory;
use Zend\Db\TableGateway\TableGateway;

class FollowDocsTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function followDocList()
    {
        $dataList = [];
        $rowset = $this->tableGateway->select();

        foreach ($rowset as $item)
        {
            $dataList[$item->followID][] = $item->docs;
        }

        return $dataList;
    }

    public function followDocs(array $data)
    {
        $dataList = [];
        $rowset = $this->tableGateway->select($data);

        foreach ($rowset as $item)
        {
            $dataList[] = $item->docs;
        }

        return $dataList;
    }

    public function saveFollowDocs(FollowDocs $item)
    {
        $data = [
            'follow_id' => $item->followID,
            'docs' => $item->docs,
            'create_date' => $item->createDate
        ];

        try{
            $result =  $this->tableGateway->insert($data);
            if($result)
            {
                return $this->tableGateway->getLastInsertValue();
            }
        }catch (Exception $e)
        {
            return $e->getMessage();
        }
    }

    public function removeFollowDocs($id)
    {
        $result = $this->tableGateway->delete(['follow_id' => $id]);
        if($result)
        {
            return true;
        }else{
            return false;
        }
    }

    public function updateFollowDocs(array $data,$id)
    {
        $result = $this->tableGateway->update($data, ['follow_id' => $id]);
        if($result)
        {
            return true;
        }else{
            return false;
        }
    }
}
