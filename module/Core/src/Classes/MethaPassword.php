<?php
/**
 * Time: 22:49
 * Description:
 */

namespace Core\Classes;

class MethaPassword
{
    public function crypt(array $data)
    {
        $email = $data['email'];
        /**
         * Diziden gelen saf parolamızı önce md5'e çeviriyoruz.
         * Bunun sebebi; Enjekte verilerini saf olarak md5'e
         * çeviriyor.Burada uygulayacağımız saf parola sonrası
         * ilk işlem md5 olacak.Sonrasında kendi algoritmamıza göre kaydedilecek.
         */
        $password = md5($data['password']);
        $createDate = $data['create_date'];

        $hashMac = hash_hmac('ripemd160', $this->guidv4(), $email.$password.$createDate);
        $output = hash('sha256',$hashMac);
        $cryptArray = array_chunk(str_split($output,1), 10, true);

        $crypt = '';
        foreach ($cryptArray as $item)
        {
            $crypt2 = '';
            foreach ($item as $key => $value)
            {
                $crypt2 = $crypt2.$value;
            }

            $crypt = $crypt.'-'.$crypt2;
        }

        $resultCrypt = substr($crypt,1);

        return $resultCrypt;
    }

    private function guidv4()
    {
        if (function_exists('com_create_guid') === true)
            return trim(com_create_guid(), '{}');

        $data = openssl_random_pseudo_bytes(16);
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }
}
