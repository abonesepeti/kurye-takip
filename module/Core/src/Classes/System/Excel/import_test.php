<?php
/**
 * User: Mehmet HAKKIOĞLU
 * Mail: mehmethakkioglu@yandex.com
 * Date: 26.04.2019
 * Time: 10:26
 */
include 'src/SimpleXLSX.php';
include "../Database/Activation.php";
$db = new Activation();

$packageType = $db->packageTypeIDList();
$packages = $db->packageIDList();
$fsTitle = $db->fsIDList();

$warnings = [];
$activationGSMTypeList = $db->activationGSMList();
//die('GÜNCEL VERİ');
if ( $xlsx = SimpleXLSX::parse('report.xlsx') ) {
    $datas = $xlsx->rows();

    unset($datas[0]);


    $query = 'INSERT INTO activation (date, gsm, status, name, surname, tc, action, package, fs, agent, channel, personel, description) VALUES ';
    foreach ($datas as $item) {
        $date = date('Y-m-d H:i:s', $xlsx->unixstamp( $item[0] ));

        $action = null;
        switch ($item[5]) {
            case 'CHURN':
                $action = 'mnt';
                break;
            case 'YENİ TESİS':
                $action = 'yenitesis';
                break;
            case null:
                $action = 'empty';
                break;
        }

        $status = null;
        switch ($item[2]) {
            case 'AKTİF':
                $status = 'Y';
                break;
            case 'DEAKTİF':
                $status = 'N';
                break;
            case 'BEKLEMEDE':
                $status = 'P';
                break;
        }

        $nameSurname = explode(' ',$item[3]);
        $surname = array_pop($nameSurname);
        $name = implode(array_slice($nameSurname,0,4),' ');

        $uniqControl = $db->activationUniqueList(sprintf('date="%s" AND gsm="%s"',$date,$item[1]));


        $fsIDFind = $item[8] == 'RDIVAN' ? 'RIDVAN' : $item[8];
        $fsName = $db->fsIDList()[$fsIDFind];
        $agent = $db->agentIDList()[$item[9]];

        /** todo: Eğer sistemde hiç bir şekilde eşleşmiyor ise yeni kayıt olarak girilecek. */
        if ($item[7] == 'RELDİ 8' || $item[7] == 'REDLİ 8' || $item[7] == 'REDL İ8' || $item[7] == 'REDLİ8') {
            if ($item[6] == 'KOÇ') {
                $query .= sprintf('("%s", %d, "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s"), ',$date,$item['1'],$status, $name,$surname,$item[4],$action,$packages[$packageType['KOÇ']]['REDLİ 8'], $fsName,$agent, $item[10], $item[11],$item[12]);
            } elseif ($item[6] == 'RED') {
                $query .= sprintf('("%s", %d, "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s"), ',$date,$item['1'],$status, $name,$surname,$item[4],$action,$packages[$packageType['RED']]['REDLİ 8'], $fsName,$agent, $item[10], $item[11],$item[12]);
            }
        } elseif ($item[7] == 'BENİM PASSİM' || $item[7] == 'BENİM PASSIM' || $item[7] == 'BENİM PASSIM ') {
            $query .= sprintf('("%s", %d, "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s"), ',$date,$item['1'],$status, $name,$surname,$item[4],$action,$packages[$packageType['RED']]['BENİM PASSIM'], $fsName,$agent, $item[10], $item[11],$item[12]);
        } elseif ($item[7] == 'REDLİ 15' || $item[7] == 'REDLİ 15 ' || $item[7] == 'REDL 15 ') {
            if ($item[6] == 'KOÇ') {
                $query .= sprintf('("%s", %d, "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s"), ',$date,$item['1'],$status, $name,$surname,$item[4],$action,$packages[$packageType['KOÇ']]['REDLİ 15'], $fsName,$agent, $item[10], $item[11],$item[12]);
            } elseif ($item[6] == 'RED') {
                $query .= sprintf('("%s", %d, "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s"), ',$date,$item['1'],$status, $name,$surname,$item[4],$action,$packages[$packageType['RED']]['REDLİ 15'], $fsName,$agent, $item[10], $item[11],$item[12]);
            }
        } elseif ($item[7] == 'REDLİ 12' || $item[7] == 'REDLİ 12 ' || $item[7] == 'REDL İ12') {
            if ($item[6] == 'KOÇ') {
                $query .= sprintf('("%s", %d, "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s"), ',$date,$item['1'],$status, $name,$surname,$item[4],$action,$packages[$packageType['KOÇ']]['REDLİ 12'], $fsName,$agent, $item[10], $item[11],$item[12]);
            } elseif ($item[6] == 'RED') {
                $query .= sprintf('("%s", %d, "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s"), ',$date,$item['1'],$status, $name,$surname,$item[4],$action,$packages[$packageType['RED']]['REDLİ 12'], $fsName,$agent, $item[10], $item[11],$item[12]);
            }
        } elseif ($item[7] == 'REDLİ 15 PLUS') {
            if ($item[6] == 'KOÇ') {
                $query .= sprintf('("%s", %d, "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s"), ',$date,$item['1'],$status, $name,$surname,$item[4],$action,$packages[$packageType['KOÇ']]['REDLİ 15 PLUS'], $fsName,$agent, $item[10], $item[11],$item[12]);
            } elseif ($item[6] == 'RED') {
                $query .= sprintf('("%s", %d, "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s"), ',$date,$item['1'],$status, $name,$surname,$item[4],$action,$packages[$packageType['RED']]['REDLİ 15 PLUS'], $fsName,$agent, $item[10], $item[11],$item[12]);
            }
        } elseif ($item[7] == 'REDLİ 20 PLUS') {
            if ($item[6] == 'KOÇ') {
                $query .= sprintf('("%s", %d, "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s"), ',$date,$item['1'],$status, $name,$surname,$item[4],$action,$packages[$packageType['KOÇ']]['REDLİ 20 PLUS'], $fsName,$agent, $item[10], $item[11],$item[12]);
            } elseif ($item[6] == 'RED') {
                $query .= sprintf('("%s", %d, "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s"), ',$date,$item['1'],$status, $name,$surname,$item[4],$action,$packages[$packageType['RED']]['REDLİ 20 PLUS'], $fsName,$agent, $item[10], $item[11],$item[12]);
            }
        } elseif ($item[7] == 'REDLİ 9') {
            if ($item[6] == 'RED') {
                $query .= sprintf('("%s", %d, "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s"), ',$date,$item['1'],$status, $name,$surname,$item[4],$action,$packages[$packageType['RED']]['REDLİ 9'], $fsName,$agent, $item[10], $item[11],$item[12]);
            }
        } elseif ($item[7] == 'SAÇMA GÜZEL ') {
            if ($item[6] == 'MASS') {
                $query .= sprintf('("%s", %d, "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s"), ',$date,$item['1'],$status, $name,$surname,$item[4],$action,$packages[$packageType['MASS']]['SAÇMA GÜZEL 4'], $fsName,$agent, $item[10], $item[11],$item[12]);
            }
        } elseif ($item[7] == 'SAÇMA GÜZEL 10') {
            if ($item[6] == 'MASS') {
                $query .= sprintf('("%s", %d, "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s"), ',$date,$item['1'],$status, $name,$surname,$item[4],$action,$packages[$packageType['MASS']]['SAÇMA GÜZEL 10'], $fsName,$agent, $item[10], $item[11],$item[12]);
            }
        } elseif ($item[7] == 'SAÇMA GÜZEL 16' || $item[7] == 'SAÇMA GÜZEL 16 ') {
            if ($item[6] == 'MASS') {
                $query .= sprintf('("%s", %d, "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s"), ',$date,$item['1'],$status, $name,$surname,$item[4],$action,$packages[$packageType['MASS']]['SAÇMA GÜZEL 16'], $fsName,$agent, $item[10], $item[11],$item[12]);
            }
        } elseif ($item[7] == 'SAÇMA GÜZEL 4') {
            if ($item[6] == 'MASS') {
                $query .= sprintf('("%s", %d, "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s"), ',$date,$item['1'],$status, $name,$surname,$item[4],$action,$packages[$packageType['MASS']]['SAÇMA GÜZEL 4'], $fsName,$agent, $item[10], $item[11],$item[12]);
            }
        } elseif ($item[7] == 'SAÇMA GÜZEL 8') {
            if ($item[6] == 'MASS') {
                $query .= sprintf('("%s", %d, "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s"), ',$date,$item['1'],$status, $name,$surname,$item[4],$action,$packages[$packageType['MASS']]['SAÇMA GÜZEL 8'], $fsName,$agent, $item[10], $item[11],$item[12]);
            }
        } elseif ($item[7] == 'UYUMLU 4' || $item[7] == 'UUYUMLU 4' || $item[7] == 'UYUMLU4' || $item[7] == 'UUYMLU 4') {
            if ($item[6] == 'MASS') {
                $query .= sprintf('("%s", %d, "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s"), ',$date,$item['1'],$status, $name,$surname,$item[4],$action,$packages[$packageType['MASS']]['UYUMLU 4'], $fsName,$agent, $item[10], $item[11],$item[12]);
            }
        } elseif ($item[7] == 'UYUMLU 1') {
            if ($item[6] == 'MASS') {
                $query .= sprintf('("%s", %d, "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s"), ',$date,$item['1'],$status, $name,$surname,$item[4],$action,$packages[$packageType['MASS']]['UYUMLU 1'], $fsName,$agent, $item[10], $item[11],$item[12]);
            }
        } elseif ($item[7] == 'UYUYMLU 2' || $item[7] == 'UYUUMLU 2' || $item[7] == 'UYUMLU 2') {
            if ($item[6] == 'MASS') {
                $query .= sprintf('("%s", %d, "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s"), ',$date,$item['1'],$status, $name,$surname,$item[4],$action,$packages[$packageType['MASS']]['UYUMLU 2'], $fsName,$agent, $item[10], $item[11],$item[12]);
            }
        } elseif ($item[7] == 'UYUMLU 3') {
            if ($item[6] == 'MASS') {
                $query .= sprintf('("%s", %d, "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s"), ',$date,$item['1'],$status, $name,$surname,$item[4],$action,$packages[$packageType['MASS']]['UYUMLU 3'], $fsName,$agent, $item[10], $item[11],$item[12]);
            }
        } elseif ($item[7] == 'UYUMLU 3') {
            if ($item[6] == 'MASS') {
                $query .= sprintf('("%s", %d, "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s"), ',$date,$item['1'],$status, $name,$surname,$item[4],$action,$packages[$packageType['MASS']]['UYUMLU 3'], $fsName,$agent, $item[10], $item[11],$item[12]);
            }
        } elseif ($item[7] == 'UYUMLU 6') {
            if ($item[6] == 'MASS') {
                $query .= sprintf('("%s", %d, "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s"), ',$date,$item['1'],$status, $name,$surname,$item[4],$action,$packages[$packageType['MASS']]['UYUMLU 6'], $fsName,$agent, $item[10], $item[11],$item[12]);
            }
        } elseif ($item[7] == 'UYUMLU 8') {
            if ($item[6] == 'MASS') {
                $query .= sprintf('("%s", %d, "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s"), ',$date,$item['1'],$status, $name,$surname,$item[4],$action,$packages[$packageType['MASS']]['UYUMLU 8'], $fsName,$agent, $item[10], $item[11],$item[12]);
            }
        }
    }

    $result = $db->testSave(substr($query,0,strlen($query) - 2));
}