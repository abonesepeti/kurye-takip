<?php
/**
 * Time: 22:49
 * Description:
 */
namespace Core\Classes;

use Follow\Module;

class MethaClasses
{
    public function slug(string $title)
    {
        $title = preg_replace('~[^\pL\d]+~u', '-', $title);
        $title = iconv('utf-8', 'us-ascii//TRANSLIT', $title);
        $title = preg_replace('~[^-\w]+~', '', $title);
        $title = trim($title, '-');
        $title = preg_replace('~-+~', '-', $title);
        $title = strtolower($title);

        if (empty($title))
        {
            return 'n-a';
        }

        return $title;
    }

    public function documentUpload(array $data)
    {
        $types = [
            'image/jpeg' => 'jpeg',
            'image/jpg' => 'jpg',
            'image/png' => 'png',
            'image/gif' => 'gif',
            'image/bmp' => 'bmp',
        ];

        $ext = strtolower($types[$data['type']]);

        $imageName = hash('sha256',date('Y-m-d H:i:s').uniqid()).'.'.$ext;
        $path = Module::FOLLOW_DOCUMENT_ROUTE.$imageName;
        $result = move_uploaded_file($data['tmp_name'], $path);
        chmod($path,777);

        if($result)
        {
            return $imageName;
        }else{
            return false;
        }
    }

    public function excelUpload(array $data)
    {
        $ext = 'xlsx';

        $imageName = hash('sha256',date('Y-m-d H:i:s').uniqid()).'.'.$ext;
        $path = Module::FOLLOW_DOCUMENT_ROUTE.$imageName;
        $result = move_uploaded_file($data['tmp_name'], $path);
        chmod($path,0777);

        if($result)
        {
            return $imageName;
        }else{
            return false;
        }
    }

    public function priceTypes()
    {
        return [
            [
                'label' => 'Aktif Fiyat',
                'name' => 'aktif'
            ],
            [
                'label' => 'Gönderi Teslim',
                'name' => 'gonderi'
            ]
        ];
    }
}
