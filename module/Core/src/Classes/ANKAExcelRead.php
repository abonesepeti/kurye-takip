<?php
/**
 * Created by PhpStorm.
 * User: Mehmet HAKKIOĞLU <mehmethakkioglu@yandex.com>
 * Date: 2019-08-21
 * Time: 22:49
 * Description:
 */
namespace Core\Classes;

use Follow\Module;
use SimpleXLSX;

class ANKAExcelRead
{
    private $file;
    private $path;

    public function __construct(string $file)
    {
        $this->file = $file;
        $this->path = Module::FOLLOW_DOCUMENT_ROUTE;
    }

    public function fileRead()
    {
        include "/home/absorg/public_html/module/Core/src/Classes/System/Excel/src/SimpleXLSX.php";
        $xlsx = SimpleXLSX::parse($this->file);

        $dataList = [];

        foreach ($xlsx->rows() as $key => $item)
        {
            if($key == 0)
            {
                $dataList['columns'] = $item;
            }else{
                $dataList['rows'][] = $item;
            }
        }

        return $dataList;
    }
}
