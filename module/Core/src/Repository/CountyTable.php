<?php
/**
 * Time: 13:22
 */
namespace Core\Repository;

use Exception;
use Follow\Model\FollowCore;
use Zend\Db\TableGateway\TableGateway;

class CountyTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function cityList()
    {
        $dataList = [];
        $rowset = $this->tableGateway->select();

        foreach ($rowset as $item)
        {
            $dataList[$item->countyCityKey][$item->countyKey] = $item->countyTitle;
            $dataList[$item->countyCityKey]['all_'.$item->countyCityKey] = 'Tüm İlçeler';
        }

        return $dataList;
    }

    public function countyToCityList()
    {
        $dataList = [];
        $rowset = $this->tableGateway->select();

        foreach ($rowset as $item)
        {
            $dataList[$item->countyKey] = $item->countyCityKey;
        }

        return $dataList;
    }

    public function titleToCounty()
    {
        $dataList = [];
        $rowset = $this->tableGateway->select();

        foreach ($rowset as $item)
        {
            $dataList[$item->countyCityKey][$item->countyTitle] = $item->countyKey;
            $dataList[$item->countyCityKey]['all_'.$item->countyCityKey] = 'all';
        }

        return $dataList;
    }

    public function countyToCity($id)
    {
        $dataList = [];
        $rowset = $this->tableGateway->select(sprintf('county_key=%d',$id));

        foreach ($rowset as $item)
        {
            $dataList = $item->countyCityKey;
        }

        return $dataList;
    }
}
