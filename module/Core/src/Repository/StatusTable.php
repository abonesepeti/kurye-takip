<?php
/**
 * Time: 13:22
 */
namespace Core\Repository;

use Zend\Db\TableGateway\TableGateway;

class StatusTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function statusList()
    {
        $dataList = [];
        $rowset = $this->tableGateway->select();

        foreach ($rowset as $item)
        {
            $dataList[$item->id] = $item->title;
        }

        return $dataList;
    }

    public function statusDetailList()
    {
        $dataList = [];
        $rowset = $this->tableGateway->select();

        foreach ($rowset as $item)
        {
            $dataList[$item->id] = [
                'title' => $item->title,
                'settings' => $item->settings
            ];
        }

        return $dataList;
    }
}
