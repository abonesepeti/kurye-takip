<?php
/**
 * Time: 13:22
 */
namespace Core\Repository;

use Zend\Db\TableGateway\TableGateway;

class CityTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function cityList()
    {
        $dataList = [];
        $rowset = $this->tableGateway->select();

        foreach ($rowset as $item)
        {
            $dataList[$item->cityKey] = $item->cityTitle;
        }

        return $dataList;
    }

    public function titleToKey()
    {
        $dataList = [];
        $rowset = $this->tableGateway->select();

        foreach ($rowset as $item)
        {
            $dataList[$item->cityTitle] = $item->cityKey;
        }

        return $dataList;
    }
}
