<?php
/**
 * Time: 22:49
 * Description:
 */

namespace Core\Model;

class City
{
    public $cityID;
    public $cityTitle;
    public $cityKey;

    public function exchangeArray($data)
    {
        $this->cityID  = (!empty($data['city_id'])) ? $data['city_id'] : null;
        $this->cityTitle = (!empty($data['city_title'])) ? $data['city_title'] : null;
        $this->cityKey = (!empty($data['city_key'])) ? $data['city_key'] : null;
    }

    public function getArrayCopy()
    {
        return [
            'city_id' => $this->cityID,
            'city_title' => $this->cityTitle,
            'city_key' => $this->cityKey
        ];
    }
}
