<?php
/**
 * Time: 22:49
 * Description:
 */

namespace Core\Model;

class County
{
    public $countyID;
    public $countyTitle;
    public $countyKey;
    public $countyCityKey;

    public function exchangeArray($data)
    {
        $this->countyID  = (!empty($data['county_id'])) ? $data['county_id'] : null;
        $this->countyTitle = (!empty($data['county_title'])) ? $data['county_title'] : null;
        $this->countyKey = (!empty($data['county_key'])) ? $data['county_key'] : null;
        $this->countyCityKey = (!empty($data['county_city_key'])) ? $data['county_city_key'] : null;
    }

    public function getArrayCopy()
    {
        return [
            'county_id' => $this->countyID,
            'county_title' => $this->countyTitle,
            'county_key' => $this->countyKey,
            'county_city_key' => $this->countyCityKey,
        ];
    }
}
