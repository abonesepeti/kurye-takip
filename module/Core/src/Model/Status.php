<?php
/**
 * Time: 22:49
 * Description:
 */

namespace Core\Model;

class Status
{
    public $id;
    public $title;
    public $settings;
    public $updateDate;

    public function exchangeArray($data)
    {
        $this->id  = (!empty($data['id'])) ? $data['id'] : null;
        $this->title = (!empty($data['title'])) ? $data['title'] : null;
        $this->settings = (!empty($data['settings'])) ? $data['settings'] : null;
        $this->updateDate = (!empty($data['update_date'])) ? $data['update_date'] : null;
    }

    public function getArrayCopy()
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'settings' => $this->settings,
            'update_date' => $this->updateDate
        ];
    }
}
