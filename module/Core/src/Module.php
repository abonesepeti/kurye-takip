<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Core;

use Core\Model\City;
use Core\Model\County;
use Core\Model\Status;
use Core\Repository\CityTable;
use Core\Repository\CountyTable;
use Core\Repository\StatusTable;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ModuleManager\Feature\ConfigProviderInterface;

class Module implements ConfigProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                CityTable::class => function ($container) {
                    $tableGateway = $container->get('Repository\CityTableGateway');
                    $table = new CityTable($tableGateway);
                    return $table;
                },
                'Repository\CityTableGateway' => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new City());
                    return new TableGateway('city', $dbAdapter, null, $resultSetPrototype);
                },
                CountyTable::class => function ($container) {
                    $tableGateway = $container->get('Repository\CountyTableGateway');
                    $table = new CountyTable($tableGateway);
                    return $table;
                },
                'Repository\CountyTableGateway' => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new County());
                    return new TableGateway('county', $dbAdapter, null, $resultSetPrototype);
                },
                StatusTable::class => function ($container) {
                    $tableGateway = $container->get('Repository\StatusTableGateway');
                    $table = new StatusTable($tableGateway);
                    return $table;
                },
                'Repository\StatusTableGateway' => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Status());
                    return new TableGateway('status', $dbAdapter, null, $resultSetPrototype);
                },
            ],
        ];
    }
}
