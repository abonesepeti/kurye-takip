<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Core\Controller;

use Core\Repository\CountyTable;
use Core\Repository\StatusTable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class APIController extends AbstractActionController
{
    /** @var $countyTable CountyTable */
    private $countyTable;

    /** @var $status StatusTable */
    private $status;

    public function __construct()
    {
        $args = func_get_args();
        $this->countyTable = $args[0];
        $this->status = $args[1];
    }

    public function apiAction()
    {
        return $this->redirect()->toRoute('site');
        $view = new ViewModel([
            'content' => ['key' => 'value']
        ]);
        $view->setTemplate('page/api/json');
        $this->layout()->setTemplate('layout/json_layout');
        return $view;
    }

    public function statusAction()
    {
        $view = new ViewModel([
            'content' => [
                'code' => 200,
                'message' => 'Başarılı',
                'result' => $this->status->statusList()
            ]
        ]);
        $view->setTemplate('page/api/json');
        $this->layout()->setTemplate('layout/json_layout');
        return $view;
    }

    public function countyAction()
    {
        $view = new ViewModel();

        if ($this->getRequest()->isPost())
        {
            $formData = $this->params()->fromPost();

            $view->setVariables([
                'content' => $this->countyTable->cityList()[$formData['city_id']]
            ]);
        }else{
            $view->setVariables([
                'content' => [
                    'code' => 403,
                    'message' => 'Yetkisiz erişim.'
                ]
            ]);
        }

        $view->setTemplate('page/api/json');
        $this->layout()->setTemplate('layout/json_layout');
        return $view;
    }
}
