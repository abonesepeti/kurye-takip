<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Core\Controller;

use Magazine\Repository\MagazineTable;
use User\Repository\UserCoreTable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class PanelController extends AbstractActionController
{
    /** @var $userCoreTable UserCoreTable */
    private $userCoreTable;

    /** @var $magazineCoreTable MagazineTable */
    private $magazineTable;

    public function __construct()
    {
        $args = func_get_args();
        $this->userCoreTable = $args[0];
        $this->magazineTable = $args[1];
    }

    public function indexAction()
    {
        $breadcrumb = [
            [
                'title' => 'Panel'
            ]
        ];
        $title = 'Panel';
        $view = new ViewModel([
            'title' => $title,
            'userCount' => count($this->userCoreTable->userList()),
            'magazineCount' => count($this->magazineTable->magazineList())
        ]);
        $view->setTemplate('page/panel/index');
        $this->layout()->setTemplate('layout/panel_layout')->setVariables([
            'title' => $title,
            'breadcrumb' => $breadcrumb
        ]);
        return $view;
    }
}
