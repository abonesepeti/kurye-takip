<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Core\Controller;

use Carrier\Repository\CarrierCoreTable;
use Core\Repository\CityTable;
use Core\Repository\CountyTable;
use Core\Repository\StatusTable;
use Follow\Repository\FollowCoreTable;
use User\Repository\UserCoreTable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class SiteController extends AbstractActionController
{
    /** @var $followCoreTable FollowCoreTable */
    private $followCoreTable;

    /** @var $carrierCoreTable CarrierCoreTable */
    private $carrierCoreTable;

    /** @var $city CityTable */
    private $city;

    /** @var $county CountyTable */
    private $county;

    /** @var $status StatusTable */
    private $status;

    /** @var $userCoreTable UserCoreTable */
    private $userCoreTable;

    public function __construct()
    {
        $args = func_get_args();
        $this->followCoreTable = $args[0];
        $this->carrierCoreTable = $args[1];
        $this->city = $args[2];
        $this->county = $args[3];
        $this->status = $args[4];
        $this->userCoreTable = $args[5];
    }

    public function indexAction()
    {
        $title = 'Ana Sayfa';
        $options = [
            'title' => $title,
            'subtitle' => '',
            'breadcrumb' => [
                [
                    'title' => 'Ana Sayfa',
                    'route' => 'site'
                ]
            ]
        ];

        $carrierUser = $this->carrierCoreTable->findByOneCarrier([
            'user_id' => $_SESSION['userInfo']['id']
        ]);

        $count = 0;
        $success = 0;
        foreach ($this->followCoreTable->followList() as $item)
        {
            if($item['user_id'] == $_SESSION['userInfo']['id'])
            {
                if($item['status'] == 9)
                {
                    $success++;
                }else{
                    $count++;
                }
            }
        }

        $carrierList = $this->carrierCoreTable->carrierList();
        $userList = [];

        foreach ($this->userCoreTable->userList() as $item)
        {
            if($_SESSION['userInfo']['type'] == 'admin')
            {
                $userList[$item['id']] = $item['id'];
            }
        }

        $userCarrierList = [];
        if($_SESSION['userInfo']['type'] == 'user')
        {
            foreach ($carrierList as $items)
            {
                foreach (json_decode($items['dealers']) as $item)
                {
                    if($item == 'all' || $item == $_SESSION['userInfo']['id'])
                    {
                        $userCarrierList[$items['user_id']] = $items['user_id'];
                    }
                }
            }
        }

        $followList = [];
        foreach ($this->followCoreTable->followList() as $item)
        {
            $date = new \DateTime($item['create_date']);
            if($_SESSION['userInfo']['type'] == 'admin' && $item['status'] != 3)
            {
                $followList[] = $item;
            }elseif($_SESSION['userInfo']['type'] == 'user' && $item['status'] != 3)
            {
                if(isset($userCarrierList[$item['user_id']]))
                {
                    $followList[] = $item;
                }
            }elseif($_SESSION['userInfo']['type'] == 'carrier' && $item['status'] != 3)
            {
                if($item['user_id'] == $_SESSION['userInfo']['id'])
                {
                    $followList[] = $item;
                }
            }
        }

        $view = new ViewModel([
            'options' => $options,
            'followList' => $followList,
            'statusList' => $this->status->statusList(),
            'carrierList' => $this->carrierCoreTable->carrierList(),
            'counts' => $count,
            'success' => $success,
            'cityList' => $this->city->cityList(),
            'countyList' => $this->county->cityList()
        ]);
        $view->setTemplate('page/site/index');
        $this->layout()->setTemplate('layout/layout')->setVariables([
            'title' => $title,
            'navbar' => 'home'
        ]);

        if($_SESSION['loginControl'] == true)
        {
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user/login');
        }
    }

    public function controlAction()
    {
        $title = 'Kurye Kontrol';
        $options = [
            'title' => $title,
            'subtitle' => '',
            'breadcrumb' => [
                [
                    'title' => $title,
                    'route' => 'site'
                ]
            ]
        ];


        $view = new ViewModel([
            'options' => $options
        ]);
        $view->setTemplate('page/site/control');
        $this->layout()->setTemplate('layout/follow_layout')->setVariables([
            'title' => $title,
            'navbar' => 'home'
        ]);

        if($_SESSION['loginControl'] == true)
        {
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user/login');
        }
    }
}
