<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Core;

use Carrier\Repository\CarrierCoreTable;
use Core\Repository\CityTable;
use Core\Repository\CountyTable;
use Core\Repository\StatusTable;
use Follow\Repository\FollowCoreTable;
use User\Repository\UserCoreTable;
use Zend\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'site'        => [
                'type'    => Segment::class,
                'may_terminate' => true,
                'options' => [
                    'route'    => '[/]',
                    'defaults' => [
                        'controller' => Controller\SiteController::class,
                        'action'     => 'index',
                    ],
                ],
                'child_routes' => [
                    'contact'        => [
                        'type'    => Segment::class,
                        'may_terminate' => true,
                        'options' => [
                            'route'    => 'contact[/]',
                            'defaults' => [
                                'controller' => Controller\SiteController::class,
                                'action'     => 'contact',
                            ],
                        ],
                    ],
                    'control'        => [
                        'type'    => Segment::class,
                        'may_terminate' => true,
                        'options' => [
                            'route'    => 'control[/]',
                            'defaults' => [
                                'controller' => Controller\SiteController::class,
                                'action'     => 'control',
                            ],
                        ],
                    ],
                ]
            ],
            'panel' => [
                'type'    => Segment::class,
                'may_terminate' => true,
                'options' => [
                    'route'    => '/panel[/]',
                    'defaults' => [
                        'controller' => Controller\PanelController::class,
                        'action'     => 'index',
                    ],
                ]
            ],
            'api' => [
                'type'    => Segment::class,
                'may_terminate' => true,
                'options' => [
                    'route'    => '/api[/]',
                    'defaults' => [
                        'controller' => Controller\APIController::class,
                        'action'     => 'api',
                    ],
                ],
                'child_routes' => [
                    'county' => [
                        'type'    => Segment::class,
                        'may_terminate' => true,
                        'options' => [
                            'route'    => 'county[/]',
                            'defaults' => [
                                'controller' => Controller\APIController::class,
                                'action'     => 'county',
                            ],
                        ],
                    ],
                    'status' => [
                        'type'    => Segment::class,
                        'may_terminate' => true,
                        'options' => [
                            'route'    => 'status[/]',
                            'defaults' => [
                                'controller' => Controller\APIController::class,
                                'action'     => 'status',
                            ],
                        ],
                    ],
                ]
            ],
        ],
    ],

    'controllers' => [
        'factories' => [
            Controller\SiteController::class => function($container) {
                return new Controller\SiteController(
                    $container->get(FollowCoreTable::class),
                    $container->get(CarrierCoreTable::class),
                    $container->get(CityTable::class),
                    $container->get(CountyTable::class),
                    $container->get(StatusTable::class),
                    $container->get(UserCoreTable::class)
                );
            },
            Controller\PanelController::class => function($container) {
                return new Controller\PanelController(
                    $container->get(UserCoreTable::class)
                );
            },
            Controller\APIController::class => function($container) {
                return new Controller\APIController(
                    $container->get(CountyTable::class),
                    $container->get(StatusTable::class)
                );
            },
        ],
    ],

    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map'             => [
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack'      => [
            __DIR__ . '/../view',
        ],
    ],
];
