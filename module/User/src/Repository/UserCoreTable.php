<?php
/**
 * Time: 13:22
 */
namespace User\Repository;

use Exception;
use User\Model\UserCore;
use Zend\Db\TableGateway\TableGateway;

class UserCoreTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function userList()
    {
        $dataList = [];
        $rowset = $this->tableGateway->select();

        foreach ($rowset as $item)
        {
            $dataList[$item->id] = [
                'id' => $item->id,
                'username' => $item->username,
                'name_surname' => $item->nameSurname,
                'cover_price' => $item->password,
                'type' => $item->type,
                'parent_id' => $item->parentID,
                'status' => $item->status,
                'create_date' => $item->createDate,
                'update_date' => $item->updateDate,
            ];
        }

        return $dataList;
    }

    public function myUsers(array $data)
    {
        $dataList = [];
        $rowset = $this->tableGateway->select($data);

        foreach ($rowset as $item)
        {
            $dataList[$item->id] = [
                'id' => $item->id,
                'username' => $item->username,
                'name_surname' => $item->nameSurname,
                'cover_price' => $item->password,
                'type' => $item->type,
                'parent_id' => $item->parentID,
                'status' => $item->status,
                'create_date' => $item->createDate,
                'update_date' => $item->updateDate,
            ];
        }

        return $dataList;
    }

    public function userActiveList()
    {
        $dataList = [];
        $rowset = $this->tableGateway->select(sprintf('status="Y"'));

        foreach ($rowset as $item)
        {
            $dataList[] = [
                'id' => $item->id,
                'username' => $item->username,
                'name_surname' => $item->nameSurname,
                'cover_price' => $item->password,
                'type' => $item->type,
                'parent_id' => $item->parentID,
                'status' => $item->status,
                'create_date' => $item->createDate,
                'update_date' => $item->updateDate,
            ];
        }

        return $dataList;
    }

    public function findByOneUser(array $data)
    {
        $dataList = [];
        $rowset = $this->tableGateway->select($data);

        foreach ($rowset as $item)
        {
            $dataList = [
                'id' => $item->id,
                'username' => $item->username,
                'password' => $item->password,
                'name_surname' => $item->nameSurname,
                'type' => $item->type,
                'parent_id' => $item->parentID,
                'status' => $item->status,
                'create_date' => $item->createDate,
                'update_date' => $item->updateDate,
            ];
        }

        return $dataList;
    }

    public function saveUser(UserCore $log)
    {
        $data = [
            'id' => $log->id,
            'username' => $log->username,
            'name_surname' => $log->nameSurname,
            'password' => $log->password,
            'type' => $log->type,
            'parent_id' => $log->parentID,
            'status' => $log->status,
            'create_date' => $log->createDate,
            'update_date' => $log->updateDate
        ];

        try{
            $result =  $this->tableGateway->insert($data);
            if($result)
            {
                return $this->tableGateway->getLastInsertValue();
            }
        }catch (Exception $e)
        {
            return $e->getMessage();
        }
    }

    public function removeUser($id)
    {
        $result = $this->tableGateway->delete(['id' => $id]);
        if($result)
        {
            return true;
        }else{
            return false;
        }
    }

    public function updateUser(array $data,$id)
    {
        $result = $this->tableGateway->update($data, ['id' => $id]);
        if($result)
        {
            return true;
        }else{
            return false;
        }
    }
}
