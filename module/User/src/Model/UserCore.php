<?php
/**
 * Time: 22:49
 * Description:
 */

namespace User\Model;

class UserCore
{
    public $id;
    public $username;
    public $password;
    public $nameSurname;
    public $type;
    public $status;
    public $parentID;
    public $createDate;
    public $updateDate;

    public function exchangeArray($data)
    {
        $this->id  = (!empty($data['id'])) ? $data['id'] : null;
        $this->username = (!empty($data['username'])) ? $data['username'] : null;
        $this->password = (!empty($data['password'])) ? $data['password'] : null;
        $this->nameSurname = (!empty($data['name_surname'])) ? $data['name_surname'] : null;
        $this->type = (!empty($data['type'])) ? $data['type'] : null;
        $this->parentID = (!empty($data['parent_id'])) ? $data['parent_id'] : null;
        $this->status = (!empty($data['status'])) ? $data['status'] : null;
        $this->createDate = (!empty($data['create_date'])) ? $data['create_date'] : null;
        $this->updateDate = (!empty($data['update_date'])) ? $data['update_date'] : null;
    }

    public function getArrayCopy()
    {
        return [
            'id' => $this->id,
            'username' => $this->username,
            'password' => $this->password,
            'name_surname' => $this->nameSurname,
            'type' => $this->type,
            'parent_id' => $this->parentID,
            'status' => $this->status,
            'create_date' => $this->createDate,
            'update_date' => $this->updateDate
        ];
    }
}
