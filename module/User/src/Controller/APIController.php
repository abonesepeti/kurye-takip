<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace User\Controller;

use Carrier\Repository\CarrierCoreTable;
use User\Model\UserCore;
use User\Repository\UserCoreTable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class APIController extends AbstractActionController
{
    /** @var $userCoreTable UserCoreTable */
    private $userCoreTable;

    /** @var $carrierCoreTable CarrierCoreTable */
    private $carrierCoreTable;

    public function __construct()
    {
        $args = func_get_args();
        $this->userCoreTable = $args[0];
        $this->carrierCoreTable = $args[1];
    }

    public function userAction()
    {
        return $this->redirect()->toRoute('site');
    }

    public function loginAction()
    {
        $view = new ViewModel();

        if ($this->getRequest()->isPost())
        {
            $formData = $this->params()->fromPost();

            $userControl = $this->userCoreTable->findByOneUser([
                'username' => $formData['username'],
                'password' => hash('sha256',md5($formData['password'])),
                'status' => 'Y'
            ]);

            if(count($userControl) > 0)
            {
                $_SESSION['loginControl'] = true;
                $_SESSION['userInfo'] = [
                    'id' => $userControl['id'],
                    'username' => $userControl['username'],
                    'name_surname' => $userControl['name_surname'],
                    'type' => $userControl['type']
                ];

                $view->setVariables([
                    'content' => [
                        'code' => 200,
                        'message' => 'Giriş başarılı.Yönlendiriliyorsunuz...'
                    ]
                ]);
            }else{
                $view->setVariables([
                    'content' => [
                        'code' => 201,
                        'message' => 'Lütfen kullanıcı bilgilerinizi kontrol ediniz.'
                    ]
                ]);
            }
        }else{
            $view->setVariables([
                'content' => [
                    'code' => 403,
                    'message' => 'Yetkisiz erişim.'
                ]
            ]);
        }

        $view->setTemplate('page/site/json');
        $this->layout()->setTemplate('layout/json_layout');
        return $view;
    }

    public function registerAction()
    {
        $view = new ViewModel();

        if ($this->getRequest()->isPost())
        {
            $formData = $this->params()->fromPost();

            if(count($this->userCoreTable->findByOneUser([
                    'username' => $formData['username']
                ])) > 0)
            {
                $view->setVariables([
                    'content' => [
                        'code' => 201,
                        'message' => 'Girmiş olduğunuz kullanıcı adı sistemde mevcuttur.'
                    ]
                ]);
            }else{
                $formData = array_merge($formData,[
                    'create_date' => date('Y-m-d H:i:s'),
                    'update_date' => date('Y-m-d H:i:s'),
                    'status' => 'Y',
                    'password' => hash('sha256',md5($formData['password'])),
                    'type' => isset($formData['type']) ? $formData['type'] : 'user',
                    'parent_id' => isset($formData['parent_id']) ? $formData['parent_id'] : '0',
                ]);

                $userCore = new UserCore();
                $userCore->exchangeArray($formData);
                $result = $this->userCoreTable->saveUser($userCore);
                if($result)
                {
                    $view->setVariables([
                        'content' => [
                            'code' => 200,
                            'message' => 'Kayıt işleminiz başarılı bir şekilde gerçekleştirilmiştir.'
                        ]
                    ]);
                }else{
                    $view->setVariables([
                        'content' => [
                            'code' => 201,
                            'message' => 'Kayıt işleminiz sırasında bir hata ile karşılaştık.Lütfen daha sonra tekrar deneyiniz.'
                        ]
                    ]);
                }
            }
        }else{
            $view->setVariables([
                'content' => [
                    'code' => 403,
                    'message' => 'Yetkisiz erişim.'
                ]
            ]);
        }

        $view->setTemplate('page/site/json');
        $this->layout()->setTemplate('layout/json_layout');
        return $view;
    }

    public function editAction()
    {
        $view = new ViewModel();

        if ($this->getRequest()->isPost())
        {
            $formData = $this->params()->fromPost();

            $result = $this->userCoreTable->updateUser([
                'name_surname' => $formData['name_surname'],
                'username' => $formData['username'],
                'update_date' => date('Y-m-d H:i:s'),
                'password' => $formData['password'] != null ? hash('sha256',md5($formData['password'])) : $this->userCoreTable->findByOneUser([
                    'id' => $formData['id']
                ])['password']
            ],['id' => $formData['id']]);

            if($result)
            {
                $view->setVariables([
                    'content' => [
                        'code' => 200,
                        'message' => 'Düzenleme işleminiz başarıyla tamamlanmıştır.'
                    ]
                ]);
            }else{
                $view->setVariables([
                    'content' => [
                        'code' => 201,
                        'message' => 'Düzenleme sırasında bir hata ile karşılaştık.Lütfen daha sonra tekrar deneyiniz.'
                    ]
                ]);
            }
        }else{
            $view->setVariables([
                'content' => [
                    'code' => 403,
                    'message' => 'Yetkisiz erişim.'
                ]
            ]);
        }

        $view->setTemplate('page/site/json');
        $this->layout()->setTemplate('layout/json_layout');
        return $view;
    }

    public function removeAction()
    {
        $view = new ViewModel();

        if ($this->getRequest()->isPost())
        {
            $formData = $this->params()->fromPost();

            $result = $this->userCoreTable->updateUser([
                'status' => 'N',
                'update_date' => date('Y-m-d H:i:s')
            ],$formData['id']);

            if($result)
            {
                $view->setVariables([
                    'content' => [
                        'code' => 200,
                        'message' => 'Silme işleminiz başarıyla tamamlanmıştır.'
                    ]
                ]);
            }else{
                $view->setVariables([
                    'content' => [
                        'code' => 201,
                        'message' => 'Silme sırasında bir hata ile karşılaştık.Lütfen daha sonra tekrar deneyiniz.'
                    ]
                ]);
            }
        }else{
            $view->setVariables([
                'content' => [
                    'code' => 403,
                    'message' => 'Yetkisiz erişim.'
                ]
            ]);
        }

        $view->setTemplate('page/site/json');
        $this->layout()->setTemplate('layout/json_layout');
        return $view;
    }
}
