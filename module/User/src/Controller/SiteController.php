<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace User\Controller;

use User\Repository\UserCoreTable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class SiteController extends AbstractActionController
{
    /** @var $userCoreTable UserCoreTable */
    private $userCoreTable;

    public function __construct()
    {
        $args = func_get_args();
        $this->userCoreTable = $args[0];
    }

    public function userAction()
    {
        $userList = [];

        foreach ($this->userCoreTable->userActiveList() as $item)
        {
            if($item['parent_id'] == $_SESSION['userInfo']['id'])
            {
                $userList[] = $item;
            }elseif($_SESSION['userInfo']['username'] == 'admin')
            {
                $userList[] = $item;
            }
        }

        $title = 'Kullanıcı Listesi';
        $options = [
            'title' => $title,
            'subtitle' => 'Kullanıcılarınızı bu sayfadan listeleyebilirsiniz.',
            'breadcrumb' => [
                [
                    'title' => 'Ana Sayfa',
                    'route' => 'site'
                ],
                [
                    'title' => 'Kullanıcı Yönetimi',
                    'route' => 'site/user'
                ]
            ],
        ];
        $view = new ViewModel([
            'options' => $options,
            'navbar' => 'user',
            'dataList' => $userList
        ]);
        $view->setTemplate('page/site/user-index');
        $this->layout()->setTemplate('layout/layout')->setVariables([
            'title' => $title,
            'navbar' => 'user'
        ]);

        $view->setVariables([
            'userInfo' => $this->userCoreTable->findByOneUser([
                'id' => $_SESSION['userInfo']['id']
            ])
        ]);

        if($_SESSION['loginControl'] == true)
        {
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user/login');
        }
    }

    public function loginAction()
    {
        $title = 'Kullanıcı Girişi';

        $options = [
            'title' => $title,
            'subtitle' => 'Bu kısımdan kullanıcı girişi yapabilirsiniz.'
        ];

        $view = new ViewModel([
            'options' => $options
        ]);
        $view->setTemplate('page/site/user-login');
        $this->layout()->setTemplate('layout/user_layout')->setVariables([
            'title' => $title,
            'navbar' => 'home'
        ]);

        if($_SESSION['loginControl'] == true)
        {
            return $this->redirect()->toRoute('site');
        }else{
            return $view;
        }
    }

    public function registerAction()
    {
        $view = new ViewModel();
        $view->setTemplate('page/site/user-register');
        $this->layout()->setTemplate('layout/layout')->setVariables([
            'navbar' => 'home'
        ]);

        if($_SESSION['loginControl'] == true)
        {
            return $this->redirect()->toRoute('site');
        }else{
            return $view;
        }
    }

    public function logoutAction()
    {
        $_SESSION[''];
        session_destroy();
        return $this->redirect()->toRoute('site');
    }

    public function calendarAction()
    {
        $view = new ViewModel([
            'navbar' => 'calendar'
        ]);
        $view->setTemplate('page/site/user-calendar');
        $this->layout()->setTemplate('layout/layout')->setVariables([
            'navbar' => 'calendar'
        ]);

        if($_SESSION['loginControl'] == true)
        {
            $view->setVariables([
                'userInfo' => $this->userCoreTable->findByOneUser([
                    'id' => $_SESSION['userInfo']['id']
                ])
            ]);
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user/login');
        }
    }

    public function calendarCreateAction()
    {
        $view = new ViewModel([
            'navbar' => 'calendar'
        ]);
        $view->setTemplate('page/site/user-calendar-create');
        $this->layout()->setTemplate('layout/layout')->setVariables([
            'navbar' => 'calendar'
        ]);

        if($_SESSION['loginControl'] == true)
        {
            $view->setVariables([
                'userInfo' => $this->userCoreTable->findByOneUser([
                    'id' => $_SESSION['userInfo']['id']
                ])
            ]);
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user/login');
        }
    }

    public function createAction()
    {
        $title = 'Kullanıcı Oluştur';
        $options = [
            'title' => $title,
            'subtitle' => 'Alt bayilerinizi buradan oluşturabilirsiniz.',
            'breadcrumb' => [
                [
                    'title' => 'Ana Sayfa',
                    'route' => 'site'
                ],
                [
                    'title' => 'Kullanıcı Yönetimi',
                    'route' => 'site/user'
                ],
                [
                    'title' => 'Kullanıcı Oluştur',
                    'route' => 'site/user/create'
                ]
            ]
        ];

        $view = new ViewModel([
            'navbar' => 'user',
            'options' => $options,
            'userList' => $this->userCoreTable->userList()
        ]);
        $view->setTemplate('page/site/user-create');
        $this->layout()->setTemplate('layout/layout')->setVariables([
            'navbar' => 'user',
            'title' => $title
        ]);

        if($_SESSION['loginControl'] == true && $_SESSION['userInfo']['type'] == 'admin')
        {
            $view->setVariables([
                'userInfo' => $this->userCoreTable->findByOneUser([
                    'id' => $_SESSION['userInfo']['id']
                ])
            ]);
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user/login');
        }
    }

    public function editAction()
    {
        $id = $this->params()->fromRoute('id');
        $title = 'Kullanıcı Düzenle';
        $options = [
            'title' => $title,
            'subtitle' => 'Alt bayinizi buradan düzenleyebilirsiniz.',
            'breadcrumb' => [
                [
                    'title' => 'Ana Sayfa',
                    'route' => 'site'
                ],
                [
                    'title' => 'Kullanıcı Yönetimi',
                    'route' => 'site/user'
                ],
                [
                    'title' => 'Kullanıcı Düzenle',
                    'route' => 'site/user'
                ]
            ]
        ];

        $datas = $this->userCoreTable->findByOneUser([
            'id' => $id
        ]);

        $view = new ViewModel([
            'navbar' => 'user',
            'options' => $options,
            'datas' => $datas,
            'userList' => $this->userCoreTable->userList(),
            'id' => $id
        ]);
        $view->setTemplate('page/site/user-edit');
        $this->layout()->setTemplate('layout/layout')->setVariables([
            'navbar' => 'user',
            'title' => $title
        ]);

        if($_SESSION['loginControl'] == true && $_SESSION['userInfo']['type'] == 'admin')
        {
            $view->setVariables([
                'userInfo' => $this->userCoreTable->findByOneUser([
                    'id' => $_SESSION['userInfo']['id']
                ])
            ]);
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user/login');
        }
    }
}
